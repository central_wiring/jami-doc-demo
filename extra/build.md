# old build instructions










2\. **LibRingClient** (or LRC) is written in
QtCore and facilitates clients' portability between operating systems.
It does not interact with the Android client.

**Building gnome client without installing LibRing and LibRingClient**

This allows you to build every component separately and to run them from
their location without having to install them. To do this, you must
first build LibRing and then tell LibRingClient where it is located.
Then, you build the LibRingClient and tell client-gnome where
LibRingClient it is located. Finally, you build client-gnome.

1.  Build the daemon as explained above.
2.  Configure the build of LibRingClient by specifying the location of
	the (non-installed) daemon with e.g.:
		 cd build && cmake .. -DRING_BUILD_DIR=$HOME/ring-project/daemon/src -DCMAKE_BUILD_TYPE=Debug

3.  Build LibRingClient by running "make".
4.  Configure the build of client-gnome by specifying the location of
	LibRingClient using the specific variable designed for it:
		cd build && cmake .. -DLibRingClient_PROJECT_DIR=$HOME/ring-project/lrc -DCMAKE_BUILD_TYPE=Debug

5.  Build client-gnome by running "make".

To run an install-less Jami, you must manually start the daemon and then
the client, e.g.:

	term1: $HOME/ring-project/daemon/bin/dring -cdp
	term2: $HOME/ring-project/client-gnome/jami-gnome --debug

Client Qt for Jami
--------------------------------------------------

## On GNU/Linux

+ LibRing and LibRingClient must be installed first. If you have not
already done so, go to the [How to Build LibRing (or
Daemon)](#how-to-build-libring-or-daemon) and [How to Build
LibRingClient (or LRC)](#how-to-build-libringclient-or-lrc) sections.

+ **Qt 5.14**: In order to use the client Qt it is necessary to have the
Qt version 5.14 or higher. If your system does not have it you can install it
[from sources or download the binary installer](https://www.qt.io/download).
To compile the LibRingClient with a locally installed Qt you need to specify
its path, for example:
```sh
cmake -DQT5_VER=5.15.0 -DQT5_PATH=/home/<username>/Qt/5.15.0/gcc_64 -DRING_BUILD_DIR=<daemon-source-path> -DCMAKE_INSTALL_PREFIX=<lrc-installation-path> -DRING_XML_INTERFACES_DIR=<daemon-source-path>/bin/dbus ..
```

#### Installing Build Dependencies

- For Debian based:
```bash
qtmultimedia5-dev libqt5svg5-dev qtwebengine5-dev qtdeclarative5-dev qtquickcontrols2-5-dev qml-module-qtquick2 qml-module-qtquick-controls qml-module-qtquick-controls2 qml-module-qtquick-dialogs qml-module-qtquick-layouts qml-module-qtquick-privatewidgets qml-module-qtquick-shapes qml-module-qtquick-window2 qml-module-qtquick-templates2 qml-module-qt-labs-platform qml-module-qtwebengine qml-module-qtwebchannel libqrencode-dev libnm-dev
```
- For Fedora:
```bash
sudo dnf install qt5-qtsvg-devel qt5-qtwebengine-devel qt5-qtmultimedia-devel qt5-qtdeclarative-devel qt5-qtquickcontrols2-devel qt5-qtquickcontrols qrencode-devel NetworkManager-libnm-devel
```

#### Build Instructions
Once LibRingClient is built you can compile the client:

```sh
cd client-qt
mkdir build
cd build
cmake .. -DQT5_VER=5.15.0 -DQT5_PATH=/home/<username>/Qt/5.15.0/gcc_64 -DLRC=<path_to_lrc> -DCMAKE_INSTALL_PREFIX=<installation_path>
make
```
Variables `QT5_VER` and `QT5_PATH` are used to specify version and path for a custom installation of Qt.

If lrc library is installed in a custom directory you can set its path with the variable LRC. Additionally you can specify built library location with `LRCLIB` (otherwise it will seach inside LRC with the suffixes `/lib`, `/build` and `/build-local`).

Then, you are finally ready to launch jami-qt in your build directory.

If you want to install it to the path provided by `CMAKE_INSTALL_PREFIX` you can run:

```sh
make install
```

#### Debugging

Compile the client and LibRingClient with `-DCMAKE_BUILD_TYPE=Debug`

#### Known linker issues

 With Ubuntu 20.04, even if the build process finish with success, the linker might give you the following message:

```sh
/usr/bin/ld: /home/<username>/Qt/5.15.0/gcc_64/lib/libQt5WebEngineCore.so: .dynsym local symbol at index 3 (>= sh_info of 3)
(...)
```
This has been solved by switching to [gold linker](https://forum.qt.io/topic/109387/gammaray-build-error-on-ubuntu):

```sh
sudo ln -sf /usr/bin/x86_64-linux-gnu-ld.gold /usr/bin/ld
```

## On native Windows

- Make sure that daemon, lrc are built first

```bash
	cd client-windows
	python make-client.py -d
	python make-client.py -b
	powershell -ExecutionPolicy Unrestricted -File copy-runtime-files.ps1
```
- ```--toolset```, ```--sdk``` options are available, as well.
- To control the version of qmake.exe, ```--qtver``` option can be used

#### Packaging on native Windows

- To be able to generate a msi package, first download and install [Wixtoolset](https://wixtoolset.org/releases/).
- In Visual Studio, download WiX Toolset Visual Studio Extension.
- Build client-windows project first, then the JamiInstaller project, msi package should be stored in ring-project\client-windows\JamiInstaller\bin\Release

Mac OS X Client for Jami
---------------------------------------------------

+ LibRing and LibRingClient must be installed first. If you have not
already done so, go to the [How to Build LibRing (or
Daemon)](#how-to-build-libring-or-daemon) and [How to Build
LibRingClient (or LRC)](#how-to-build-libringclient-or-lrc) sections.


#### Other Requirements

-   Qt5 (we link against Qt5Core, Qt5Widgets, Qt5Gui)
-   Cocoa framework and Xcode toolchains

#### Getting the Source Code

```bash
git clone https://review.jami.net/ring-client-macosx
```

#### Build Instructions

```bash
mkdir build && cd build
export CMAKE_PREFIX_PATH=<dir_to_qt5>
```

##### Setting up Compilation with XCode

Generate an Xcode project with CMake:

```bash
cmake ../ -DCMAKE_INSTALL_PREFIX=<libringclient_install_path> -G Xcode
open Ring.xcodeproj/
```


Build and run it from Xcode.

##### Setting up Compilation by Command Line

```bash
cmake ../ -DCMAKE_INSTALL_PREFIX=<libringclient_install_path>
make
open Ring.app/
```


+ The app built using
'make' contains only links to required libraries. To fully build and
package as a standalone Bundle, see the **Packaging** section.

##### Debugging

For now, the build type of the client is "Debug" by default. However it
is useful to also have the debug symbols of LibRingClient. To do this,
specify `-DCMAKE_BUILD_TYPE=Debug` when compiling LibRingClient in the
cmake options.

##### Packaging

To make a standalone Bundle we use a cmake module:
[BundleUtilities](https://cmake.org/cmake/help/v3.0/module/BundleUtilities.html).
All dependencies are copied inside the Bundle and links are fixed.

We can then generate a "DragNDrop" dmg file with
[CPack](https://cmake.org/Wiki/CMake:Packaging_With_CPack).

```bash
cmake ../ -DCMAKE_INSTALL_PREFIX=<libringclient_install_path>
make install -j
cpack -G DragNDrop Ring
```
