# Release Process

Each Ring sub-project has its own repository, build process, integration
cycle and so on. More over the **Ring architecture is split into two
independent modules**: LibRing *(daemon)* and clients.

Having a unique revision is not a solution in this situation. The
retained idea is having a global "state" and **various updates per
module**.

For consistency, **each Ring module has to follow the same process** as
described in following points. But not all modules have to be modified
in same time.

------------------------------------------------------------------------

**PROCESS FLOW:**

1 | 2 | 3 | 4 | 5 | 6
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
Redmine Ticket |Repository Preparation |Testing |Push tags |Packaging | Advertisement

------------------------------------------------------------------------

Redmine Ticket
--------------

Create a new Task on redmine attached to the release story, for the
right sub-module. Set the title to "Release Major.Minor.Micro", with the
appropriate version number.

Repository Preparation
----------------------

**This section was outdated and removed**

Testing
-------

* Remove any existing Ring installations from your machine.
* Start with clean git tree by running `git clean -d -f -x` from the top
level directory of the project.
* Build and install the daemon and client, see How\\\_to\\\_build
* Run the test suite in daemon and client, on different distributions and
machines.
* Run manual tests
	* Try registering and using different accounts.
	* Try making calls between Ring and other free softphones (Ekiga,
Linphone), as well as hardware VoIP phones.
	* To catch uninitialized values being used, memory leaks, invalid frees,
etc. run `valgrind --track-origins=yes --db-attach=yes ./bin/dring`

Push tags
--------

`git push --tags`

Packaging
---------

```bash
git clone ssh://tcohen@gerrit-ring.savoirfairelinux.com:29420/ring
cd ring
git checkout packaging-releases
```

### RPM

```bash
vim ring-daemon.spec
```
```bash
%define version     2.2.0
%define release     1
...
...
...
%changelog
* Tue Apr 14 2015 Thibault Cohen <thibault.cohen@savoirfairelinux.com> - 2.2.0-1
- New upstream version
```

### DEB

```bash
vim debian/changelog
```
```bash
ring-daemon (2.2.0-1) unstable; urgency=medium

  [ Thibault Cohen]
  * New upstream version

 -- Thibault Cohen <thibault.cohen@savoirfairelinux.com>  Tue, 14 Apr 2015 12:40:24 -0400
```

### Release

You just have to launch release script. This script launch build,
download and update files and repositories...

```
sflvault connect 525
...
...
cd /root/repos/ring/
./ring-release-daemon.sh
```

## Gnome

```bash
git clone ssh://tcohen@gerrit-sflphone.savoirfairelinux.com:29420/ring-client-gnome
cd ring-client-gnome
git checkout packaging-releases
```

### RPM

vim ring-daemon.spec

```bash
%define version     0.2.1
%define release     1
%define daemon_tag  2.1.0
%define lrc_tag     0.2.1
%define gnome_tag   %{version}
...
...
...
%changelog
* Tue Apr 14 2015 Thibault Cohen <thibault.cohen@savoirfairelinux.com> - 0.2.1-1
- New upstream version
```

### DEB

```bash
debian/changelog
```

```bash
ring-gnome (0.2.1-1) unstable; urgency=medium

  [ Thibault Cohen]
  * New Upstream version

 -- Thibault Cohen <thibault.cohen@savoirfairelinux.com>  Tue, 14 Apr 2015 13:16:38 -0400
```

```bash
debian/rules
```

```bash
DAEMON_TAG = 2.1.0
LRC_TAG = 0.2.1
GNOME_TAG = $(VER)
```

### Release


You just have to launch release script. This script launch build, download and update files and repositories...

```bash
sflvault connect 525
...
...
cd /root/repos/ring/
./ring-release-gnome.sh
```


------------------------------------------------------------------------

Advertisement
-------------

When the packaging is finished, test that they are installable. Then
announce the release

* on the official website <https://ring.cx>
* on Twitter <https://twitter.com/JoinTheRing>
* by email to ring@lists.savoirfairelinux.net with the subject line: "Ring
Major.Minor.Patch released"
