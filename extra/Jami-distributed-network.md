# The Jami Network

### Connectivity

Jami relies on a distributed network, that brings multiple
advantages when compared to federated networks:

-   No point of failure,
-   More resilient to censorship,
-   Do not depend on anything other than its users,
-   Trust amongst nodes is not necessary.

![Network-topo](../media/distributed-network-topo.png)

This network forms a Distributed Hash Table (DHT)

The core problem of distributed communication systems is peer
connectivity, Jami achieves it through two elements:

-   Encrypted announcements on DHT,
-   Use of standard protocols for NAT hole punching.

Jami is built on two distinct distributed networks:

-   the OpenDHT kademlia network to provide distributed connectivity
	establishment and message distribution,
-   the JamiNS blockchain to provide distributed name registration.

The OpenDHT network
-------------------

See
[<https://github.com/savoirfairelinux/opendht>](https://github.com/savoirfairelinux/opendht)
for more information about OpenDHT, which provides a distributed
key-value datastore for connectivity establishment (with ICE) and
message distribution in Jami.

An OpenDHT network can be joined by knowing about any node already
connected to the network. This node will then share its knowledge about
other nodes on the network.

Jami clients use a persistent node cache to reconnect to the network
after a first session. A configurable, known, stable "bootstrap" node is
used for the first connection or if cached nodes don't answer.

Jami clients currently use bootstrap.jami.net:4222 as the default
(configurable) bootstrap node and network ID 0 (the default, public
OpenDHT network).

### Contribute to the OpenDHT network

Every Jami account runs an OpenDHT node, contributing to the network and
allowing Jami to scale.

Jami users can have full independence by running their own stable
OpenDHT node and configure it as a bootstrap node in Jami, while helping
to improve stability, robustness and resilience for every user of the
public OpenDHT network.

A standalone node can be run using the [dhtnode
utility](https://github.com/savoirfairelinux/opendht/wiki/Running-a-node-with-dhtnode)
included with OpenDHT. dhtnode doesn't persist any data and has a
default in-memory storage limit of 8 MiB.

Stable community-run DHT nodes will be added to the default bootstrap
list at the request of their owner, as more bootstrap nodes means a more
resilient, independent network.

The JamiNS blockchain
---------------------

The JamiNS blockchain is experimental and its architecture is expected
to evolve.

Jami clients don't run blockchain nodes themselves but rather
communicate with a JamiNS server using HTTP for name registration and
query, with a REST API. This is because the resources needed to run a
blockchain node are too high for most end-users.

The nameserver can be configured by-account in Jami, allowing to connect
Jami clients to various more or less centralized user directories.

### Contribute to the JamiNS blockchain

The default Jami name service is ns.jami.net, provided by Savoir-faire
Linux, connected to an Ethereum blockchain node; the goal being to give
everyone the possibility (if they which so) to run their own blockchain
node and HTTP service, mine some Ether, and use it to pay the
transaction fee needed to register their username in Jami.

Code of the Ethereum contract, the blockchain genesis file, and the
NodeJS module (HTTP server) can be found here :
[1](https://github.com/savoirfairelinux/ring-nameservice)

### Running a Jami Node

#### Pre-requisites:
1. Geth 1.8.23+ (download from [HERE](https://geth.ethereum.org/downloads/))
2. Jami genesis file (download from [HERE](https://github.com/savoirfairelinux/ring-nameservice/blob/master/instructions/genesis.json))


#### Joining the Jami Network

The process of joining the Jami network is similar to the process of joining a regular ethereum network with the difference that the genesis file is used to initialize the data directory.

1. Download the Jami genesis file from the Jami github repo
2. Create a directory on your computer which you will use to store the Jami blockchain data
  * Example for MacOS (/Users/username/jamichain)
  * Example for Linux (/home/username/jamichain)
  * Example for Windows (C:\Users\username\jamichain)
3. Use geth to initialize the directory you created in (2) by running ```./geth --datadir /home/username/jamichain  init genes is.json ```
4. You can now start geth with the command-line options you need and specifying one of Jami's bootnodes as follows:

```
geth --datadir=/home/username/jamichain --syncmode=full --networkid 1551 --bootnodes "enode://11ba6d3bfdc29a8afb24dcfcf9a08c8008005ead62756eadb363523c2ca8b819efbb264053db3d73949f1375bb3f03090f44cacfb88bade38bb6fc2cb3d890a5@173.231.120.228:30301" console
```
This will start a geth daemon with an attached console which is now syncing with Jami's network.
