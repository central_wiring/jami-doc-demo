# chatview internationalization

Note: Had to delete broken image links; page is probably out of date

The GNOME client's chatview lacks proper i18n support.

Related bug report: https://git.jami.net/savoirfairelinux/ring-client-gnome/issues/900

first ideas:

* i18n functions provided by separate js library
* i18n not trivial, overhead of custom code extremely high => use existing js lib
* library will be embedded into the client => code base should be as mature as possible
* ideally the translation process would be exactly the same as for the C++ code

# in short

(1) either

C++ tells JS code which language is currently used and JS code loads translations

(2) or

C++ loads translations and passes them to JS code together with info about currently used language

# review exisiting js i18n libs

## [i18next](https://www.i18next.com/)

**The good**

* very mature
* huge user base
* lots of documentation
* No runtime dependencies

**The bad**

* huge, overkill ?
* uses own JSON format not gettext .mo

This will require translation of .po files to JSON format (using [i18next-gettext-converter](https://github.com/i18next/i18next-gettext-converter)).

**The ugly**

* keys do not have the same meaning as gettext keys (see [this thread](https://stackoverflow.com/questions/19403787/gettext-style-keys-with-i18next-and-general-workflow)), this might be very confusing for the translators
* i18next-gettext-converter requires nodeJS as build dependency

## [jed](http://messageformat.github.io/Jed/)

**The good**

* looks quite mature
* good user base
* provides gettext API (will be very intuitive to translators)
* No runtime dependencies

**The bad**

* not very active currently (but it might very well just be stable)
* uses own JSON format not gettext .mo

This will require translation of .po files to JSON format (e.g. using [po2json](https://github.com/mikeedwards/po2json)).
po2json also requires nodeJS as dependency, **but** it is [available as Debian package](https://packages.debian.org/stable/javascript/node-po2json), which makes it slightly less annoying

Source: [Internationalization in JavaScript](http://www.jeromesteunou.net/internationalisation-in-javascript.html)

## [Polyglot](http://airbnb.io/polyglot.js/)

**The good**

* developed by airbnb, well maintained
* good user base

**The bad**

* uses own JSON format not gettext .mo

**The ugly**

* runtime dependency on nodeJS

# using Jed

* patch draft:

https://gerrit-ring.savoirfairelinux.com/c/ring-client-gnome/+/10615

* Retrieve strings:

```
$ cd po/chatview
$ xgettext -o ring-client-gnome-chatview.pot -L Javascript --from-code=utf-8 -D ../../ -f POTFILES.in

```

* Translate them:

```
$ cp ring-client-gnome-chatview.pot en.po
$ cp ring-client-gnome-chatview.pot fr.po
# translate ...
```

* Install po2json:

```
$ apt-get install node-po2json

```

* Generate JSON files:

```
$ ./scripts/build-chatview-locales.sh web/i18n
```

That's it. JSON translations are loaded as gresource. The `webkitchatcontainer` class calls `init_i18n()`, passing appropriate translation resources to the chatview.
