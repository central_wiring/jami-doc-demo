##########
Extra
##########

These are documents that either need to be finished or haven't been
categorized yet:

.. toctree::
   :maxdepth: 1
   :glob:

   guidelines/*
   ./*
