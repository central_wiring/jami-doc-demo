# Important RFCs

## SIP

Reference: <http://tools.ietf.org/html/rfc3261>

Valuable updates and extensions:

+  SIP Re-INVITE: <http://tools.ietf.org/html/rfc6141>

## ICE

Reference: <http://tools.ietf.org/html/rfc5245>

## SDP

Reference: <http://tools.ietf.org/html/rfc4566>

How to use SDP: <http://tools.ietf.org/html/rfc3264>
Valuable updates and extensions:

+  SDP and IPv6: <http://tools.ietf.org/html/rfc6157>
+  SDP for SRTP: <http://tools.ietf.org/html/rfc4568>


## RTP

Reference: <http://tools.ietf.org/html/rfc3550>

Valuable updates and extensions:

+  RTP and RTCP on same port: <http://tools.ietf.org/html/rfc5761>


## SRTP

Reference: <http://tools.ietf.org/html/rfc3711>

Valuable updates and extensions:

+  DTLS for SRTP: <https://tools.ietf.org/html/rfc5763> et <http://tools.ietf.org/html/rfc5764>
+  AES-192 and AES-256: <https://tools.ietf.org/html/rfc6188>
+  AES-GCM: <https://tools.ietf.org/html/rfc7714>
