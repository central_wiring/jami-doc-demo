# GSOC

The Ring project has previously participated in the Google Summer of
Code program on 2016 and 2017 under the umbrella of the [Debian
project](https://www.debian.org/) and the [GNU
project](https://www.gnu.org/), mentoring 7 students during these two
editions.

This year we are willing to participate as an individual organization.
Thus, we are welcoming every contribution to [Ring's source
code](https://gerrit-ring.savoirfairelinux.com/#/admin/projects/) that
could help to further develop Ring as well as our community. You can
also contribute to Ring's documentation.

### Project ideas:

#### A Node.js client for Ring

#### Ring account provisioning server

#### Auto video quality: Optimizing the auto video quality algorithm (RTCP/RTSP feedback)

Likely mentor(s): Olivier, Philippe

	For the moment ring uses a slider to set video stream quality disregarding the connection quality.
	The student will explore the RTCP and RTSP protocols implementation in FFmpeg to later implement an automation of the video stream quality setting, though the informations furnished by the control packets.
	- Renegotiate SDP depending on network conditions (change codec parameters or codecs, limit data flow, etc)
	- Improve algorithm so it can maximize quality without going over a maximum bit rate, or maintain a certain quality regardless of network traffic
	- Use other RTCP facilities to improve video quality (Ring only uses packet loss)

#### Ring IoT

Likely mentor(s): Anthony

	The ring-client-iot project, started last year by a previous GSoC student, can be used as a base for a headless Ring client. Using it to start a new client, more focused
	on little devices without direct human interaction, would enable new applications in the embedded field. From this point, multiple directions can be followed as a GSoC
	project :
	- Build Ring daemon and client-iot on ARM platforms (possibly including it in the Buildroot ecosystem).
	- Add commands to read sensor values, trigger actions on system and eventual peripherals, etc.
	- Improve manageability of accounts authorized to send commands to the device.
	- Integrate the recent file transfer feature with IoT applications (display any picture received by Ring on a screen, etc.)

#### Add video surveillance features to Ring

Likely mentor(s): Maxim, Nicolas

	The goal of this project is to enable remote video surveillance capabilities in Ring which would allow someone to keep an eye on their home, a child, etc. When a sound or movement event is triggered, a notification should be sent and the video should be recorded and stored on both the local and remote device to provide forensic evidence.

	Tasks:

	- Implement/integrate movement/sound detection algorithms in the Ring daemon.
	- Leverage the features already present in Ring to implement the message notifications as well as the video streaming and recording.
	- Design and implement the UI elements of at least one client that will define how this functionality is enabled and used.

#### Refactoring User Interfaces

#### Continue working on Telepathy client

Likely mentor(s): Olivier

	A previous work was initiated in 2016 for the integration of Ring in Telepathy [https://github.com/alok4nand/telepathy-bell], WIP:
	1. Improving the Account Management, text messaging, and Contact Management: Making the connection  manager usable as a Ring client for text messaging for a non tech user

	2. Exploring video calls: Currently the daemon handles video one frame at a time (not a video stream). If using dbus the actual frames are shared via shm (shared memory), would the video via frames model work with Telepathy/Empathy?

	3. Implement trust requests (ie: friend requests)

#### Ring as a WebRTC service

Likely mentor(s): Guillaume

	For all communications LibRing uses simple system sockets (mainly UDP, and TCP for data transfer).
	The idea is to add a WebRTC connectivity to extend Ring and facilitate portage of Ring.
	This project needs to add new settings to indicate how and when to use WebRTC sockets and re-design most of libring low-level implementations.

	Example of tasks to be done:
	- Identify impacted code.
	- Make a WebRTC small client as validation test.
	- Implement WebRTC connectivity at low-level.
	- Change libring API and settings if needed.
	- Modify/Test a currently supported client to validate the full chain.

#### Peer to peer file transfer

Likely mentor(s): Guillaume

	Current code base uses a TURN only connection (TURN/TCP/TLS) to manage a peer connectivity for our reliable data transfer feature.
	TURN is a relay protocol to solve connectivity issues between peers behind NAT or firewall. The side effects are latency and non-scalability.

	To reduce these effects we want to introduce a non-relay connection, true P2P, usable for hosts on the same network as example.
	To make connection transparent to the application (so to the user), the true P2P-way is tried as first stage and the TURN-way is used as fallback.
	This change doesn't require to change the libring API. It's only an implementation change. The data-transfer protocol (i.e. data exchanged over DHT at connection request) could be changed.

	Example of tasks to be done:
	- Identify impacted code.
	- Create a validation test.
	- Implement the true P2P in data-transfer code.
	- Support the TURN fallback.
	- Verify the data-transfer using validation tests created earlier.

#### Conference server

#### Matrix integration

Likely mentor(s): Pierre, Anthony

In the same spirit as <https://github.com/matrix-org/matrix-appservice-irc> or <https://github.com/jfrederickson/matrix-xmpp-bridge>, the goal of this project is to integrate Ring to Matrix as well as possible.
That way, communication between users of Ring and users of Matrix would be possible.

#### Redesign of the media system

Likely mentor(s): Philippe, Andreas

	Ring's media system is massive and needs an overhaul.
	The goals of this project are:
	- Implement zero-copy video frame buffer manipulations (ex: OpenGL/OpenCL, VDPAU).
	- Update the usage of Ring's media APIs and codecs to conform to current standards,
	- Provide a cleaner and better design for platform specific implementations and reduce conditionally compiled code clutter.
	- Extend the audio API to allow for client implementations on platforms where media device access is restricted to client specific APIs.

#### Create a clean wrapper for AndroidTV's API.

Likely mentor(s): Pierre, Adrien

	For now, Ring is the only video chat client available on AndroidTV. This client is more basic than the Android one mainly because it's hard to get good quality and maintainable code with the current state of the AndroidTV framework.
	Right now, the Ring AndroidTV client contains a wrapper for AndroidTV's API, but there's still boilerplate code.
	The goals of this project are:
	- To identify improvements that can be made on this wrapper
	- To create an API for Leanback easily usable in the context of Ring (will leverage any knowledge on design patterns and API design)
	- To implement the new API in Ring
	- To create an external library, independent of Ring and usable in the context of any AndroidTV application.

------------------------------------------------------------------------

### Contributions GSoC 2016:

1.  Improving distributed and secure communication using free software
	[(link)](https://summerofcode.withgoogle.com/archive/2016/projects/4886025126019072/)
2.  Indexation over a distributed network
	[(link)](https://summerofcode.withgoogle.com/archive/2016/projects/6573755878866944/)
3.  Ring project
	[(link)](https://summerofcode.withgoogle.com/archive/2016/projects/6477112403820544/)
4.  Telepathy Connection Manager for Ring protocol
	[(link)](https://summerofcode.withgoogle.com/archive/2016/projects/5047255782391808/)

### Contributions GSoC 2017

1.  Setting up unit tests for SIP calls in Ring
	[(link)](https://summerofcode.withgoogle.com/archive/2017/projects/5836252280520704/)
2.  Ring - Create a C++ plugin for Ring
	[(link)](https://summerofcode.withgoogle.com/archive/2017/projects/5704614754123776/)
3.  Ring: NodeJS Plugin for Seamless Cross-platform Client Development
	[(link)](https://summerofcode.withgoogle.com/archive/2017/projects/6532521776906240/)
