# Gérer le HDMI-CEC
Au 1er Mars 2018:

-   CEC est une norme encore sous utilisée et chaque constructeur en
	fait sa propre version
	-   max compat : <http://libcec.pulse-eight.com/Vendor/Support>
-   Framework android natif très élémentaire (api) et pas encore
	implémenté par les constructeurs
-   Libcec est un projet de Kodi qui permet le contrôle en cec
	-   les kernels android ne sont pas compatibles
	-   nécessite un accès root/adb
	-   même kodi ne fonctionne pas en cec, libcec a été retiré du build
		android
	-   <http://kodi.wiki/view/NVIDIA_SHIELD_TV#Known_issues_and_limitations>
-   Il existe des projets de driver permettant d'utiliser le cec sur
	android, mais ils nécessitent tous de flasher l'appareil
-   Il est possible d'utiliser un dongle pour envoyer les commandes cec
	en usb puis usb -&gt; hdmi
	-   coûte 45 usd sans garantie de fonctionnement
	-   <https://stackoverflow.com/questions/45639210/use-libcec-usb-dongle-in-android-app>
	-   <https://www.pulse-eight.com/p/104/usb-hdmi-cec-adapter>
-   La notion de user space driver pourrait nous servir pour écrire
	notre propre pilote, mais elle n'existe pas sur AndroidTV
