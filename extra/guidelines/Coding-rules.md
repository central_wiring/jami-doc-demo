# Coding Rules

**This page gives rules and/or guidances to all
developers that want to integrate some code to Jami **

C++ format rules are defined by this clang-format file:
https://git.jami.net/savoirfairelinux/ring-daemon/blob/master/.clang-format

All developers are recommended to format their code using the script in `jami-project/scripts/format.sh`.
This is done automatically (as a pre-commit hook) when using `./make-ring.py --init`
