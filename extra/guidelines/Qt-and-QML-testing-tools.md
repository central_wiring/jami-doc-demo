# C++

## Google Test
Google's c++ test framework.

### Installation
- Ubuntu / Debian:
`apt install googletest libgtest-dev`

## Example main.cpp
```
#include <gtest/gtest.h>

TEST(Test, Test1)
{
    EXPECT_EQ(0, 0); // OK
    EXPECT_EQ(1, 0); // ERROR and continues
    ASSERT_EQ(0, 0); // OK
    ASSERT_EQ(1, 0); // ERROR and stops execution
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();  // Runs Test1 and any other included test
}
```

# QML 

## QtQuickTest

### Installation
- Ubuntu / Debian: `apt install qml-module-qqtest libqt5quicktest5` 

- Example main.cpp
```
#include <QtQuickTest/quicktest.h>
#include <QQmlEngine>

class Setup : public QObject
{
    Q_OBJECT

public:
    Setup() {}

public slots:

    void qmlEngineAvailable(QQmlEngine *engine)
    {
        // Code to be run before the tests
    }
};

QUICK_TEST_MAIN_WITH_SETUP(testqml, Setup)
#include "main.moc"
```