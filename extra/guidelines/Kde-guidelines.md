# Coding Style

-   Indentation using 3 spaces
-   0 Indentation for preprocessor
-   Class declaration intentation = 0 space for
    public/private/protected/Q\_SIGNALS, 3 space for the methods,
    friends and attributes
-   Every function/method need a oneliner doxygen description using
    "///"
-   Function need to be ordered by 1) usage (history, contact, slots) 2)
    alphabetical order
-   Lines related to each other (like many setter on the same object
    need to be aligned down to the ";"
-   Oneliner if-elseif/switch/connect/tables need to have an header
    using /\* HEADER \*/ where \*/ close of the end of the table, also
    align the lines
-   Header file function need to be classified and aligned
-   License header is necessary for all source files
-   Attribute are named with prefix "m\_", then "s" for static, then "p"
    for pointer, "l" for list, "h" for hash, "f" for function/lambdas
    then the first letter is uppercase
-   Includes are located in the .cpp and only "class" and "namespace"
    are declared in the .h (where applicable)
-   Includes are sorted by libraries (Qt -&gt; QtCore/QtGui/QtDbus, KDE
    -&gt; Akonadi/KABC, Dring)
-   Setters take const references as parameter
-   Destructor are always virtual
-   C++11 is allowed
-   Every class need a oneliner "///@class <Classname> <description>"
    description
-   Use QDebug
-   Align everything as much as possible
-   one line "if" block and "()?:;" are allowed and recommended
-   Minimize the number of lines, but add as many white lines are
    necessary
-   Header ifnedef need to be the class upper name with upper replaced
    with \_ and \_H as suffix
-   functions/methods need to end with } //function name (more than 10
    lines only)
-   "using" (namespace keyword) usage is prohibited
-   Every \#endif need to make explicit what it is closing "\#endif
    MY\_CLASS\_H"
-   Code block have the "{" at the end of line while mothods use "\\n{"
-   Use enum class where possible
-   Always use the Full::Path for enums
-   Expose library objects as "real" qobjects (with Q\_PROPERTY,
    Q\_ENUM, Q\_FLAGS, Q\_INTERFACE and Q\_DECLARE\_NAME)
-   Follow Qt getter/setter naming conventions
-   Follow Krazy2 advices
-   string should be stored in a static struct or class as "constexpr
    static const char\*". C++14 will add
-   daemon constants should always be stored in the .h in pure static
    structs (with embedded structs to emulate namespace)
-   avoid keeping maps and list around when they represent an object.
-   transform string into enum class literal or object as soon as
    possible in the code
-   use const and static variables in methods where applicable
-   Classes should not expose multiple mutually exclusive boolean
    properties and use enum classes instead. (Example: isAudioOnly()
    and hasVideo())
-   Friendship ("friend" keyword) is accepted between the model and
    their data object to avoid exposing factory interfaces
-   Private classes declaration need to be at the top of the CPP file
-   Always an empty line before and after if/else, for, while, do,
    lambda

# Necessary Constraints

-   D-Pointer (public class pointer to private structure) need to be
    called d\_ptr <http://qt-project.org/wiki/Dpointer>
-   Q-Pointer (not to be confused with QPointer) need to be called
    q\_ptr <http://qt-project.org/wiki/Dpointer>
-   If a private header is necessary, then add it to the private folder
    with a classname\_p.h name. Do not install those, never \#include
    them in other .h, only .cpp

# Design Guideline

-   No hack
-   No logic that belong to the daemon
-   Use the state machine, don't add useless functions
-   Coherent code is not a very high priority, but avoid making things
    worst
-   Avoid using dbus call outside of the library, if you have to, your
    doing it wrong

# Current Design

```
=========================QML plasmoid====== <- Abstact frontend using the dataengine model
=====KDE frontend=========Dateengine======= <- Library frontend
==============KDE library================== <- Common GUI code and contact implementation
=============libringclient================= <- Dbus to C++ object + state machine + frontend model, -GUI LESS-
==================DBUS=====================
==================DRing====================
==================PJSIP====================
===================UDP=====================
=================SERVER====================
```