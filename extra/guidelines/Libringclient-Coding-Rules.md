**This page gives rules and/or guidances to all
developers that want to integrate some code to the LibRingClient (a
Daemon Middleware for Desktop Clients).**

-   Following rules apply to this sub-project only. Others may re-use
    this one in parts or whole, have their own, etc. Refer to their
    pages for that.

<!-- -->

-   Some rules are <u>strict</u> and are **strongly marked** or using
    modals like SHALL or MUST.


**!!!Not respecting these rules results in code-review rejection!!!**

-   Some are guidance or recommendation (using modals like SHOULD or
    RECOMMEND), it's up to the developer to balance them with his own
    way to write code.

<!-- -->

-   Rules are based on following well known ones, we've modified them
    for this project:
    -   [Google C++
        CodingStyle](https://google.github.io/styleguide/cppguide.html)
        for the general C++ domain
    -   [Qt Coding Conventions](https://wiki.qt.io/Coding_Conventions)
        and [Qt Coding Style](https://wiki.qt.io/Qt_Coding_Style) as
        this project use QtCore as framework.

<!-- -->

-   This wiki is a permanent Work-In-Progress system. **So not all rules
    are written yet.** That doesn't mean we don't have them yet (we =
    core developer members).

<!-- -->

-   We've changed some rules during the project. We update the code
    iteratively, so tt's extremly possible to find whole files or parts
    of code that not using these rules.

In such situation, if you need to change only few lines in a "logic"
block (i.e. a class declaration, a function body, ...) keep the local
rules to make easier the review: we can see what's the important change
without being disturbed by coding rules refactoring. If you need such
action, use a dedicated patch with a clear mention in the commit
message.

---

## Rules

### **Language Standard**

We SHALL use the **C++14 standard** syntax only.

For gnu-gcc and clang, use compiler command line option `-std=c++14` to
enforce this rule.

### **Maximum Line Length**

You SHOULD keep your line length under the **120 characters limit**.

This limitation is mainly due that integrators used splited view to
compare files, and keep it below 120 cols make easier to compare them,
even on wide screen. Configure your editing tools to this limit.

If keeping this rule make the code worst to read or non-valid than
exceed the limit, the rule can be transgressed. These why we've
**exceptions** to this rule:

-   If a comment line contains an example command or a literal URL
    longer than 120 characters, that line may be longer than 120
    characters for ease of cut and paste.
-   A raw-string literal may have content that exceeds 120 characters.
    Except for test code, such literals should appear near top of
    a file.
-   An \#include statement with a long path may exceed 120 columns.

### **Non-ASCII Characters**

Non-ASCII characters should be rare, and MUST use **UTF-8** formatting.

You SHALL NOT use the C++11 char16\_t and char32\_t character types,
since they're for non-UTF-8 text. For similar reasons you also SHALL NOT
use wchar\_t (unless you're writing code that interacts with the Windows
API, which uses wchar\_t extensively).

### **Spaces vs. Tabs**

In code files (i.e. .cpp or .h), you SHALL use **spaces to indent**.

One indent level is 4 (four) consecutive spaces. You should set your
editor to emit spaces when you hit the tab key and when you save your
file.

### **Testing**

Each patch for LRC should be accompanied by a test. CppUnit is used to
write tests.