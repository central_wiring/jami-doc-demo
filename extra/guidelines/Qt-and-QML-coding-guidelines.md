# Qt/c++
## Signal and slot naming
Both signals and slots should use camelCase.
A signal should use the simple past tense or past participle of some verb, likely prefixed by a short subject. A corresponding slot should be the signal prefixed with the word "on" and not the word "slot". Here are some examples:
```c
// correct
signal:
    void shutdownScheduled();
    void navigationRequested();
...
slot:
    void onShutdownScheduled();
    void onNavigationRequested();

// incorrect
signal:
    void shutdown();
    void navigateToHomeView();
...
slot:
    void onShutdown();
    void slotNavigateToHomeView();
```

---
# QML
# Code formatting
The Qt 5.15.0 version of qmlformat has some issues dealing with comment sections and currently does not discriminate against max columns, so we will continue to format using these guidelines for now.
The following is a comprehensive sample component, adapted from https://doc.qt.io/qt-5/qml-codingconventions.html, that attempts to illustrate the ideally formatted component.
```javascript
/*
 * authored by/copyright ...
 */

// Id always on the first line, and always present and called root
// for root component. Only present if referenced for children.
// Multi-line comments use '//'.
// There should always be a brief description of a component.
// Multi-line comments start with a capital letter and end with
// a period.
Rectangle {
    id: root

    // property declarations
    property bool thumbnail: false                          
    property alias image: photoImage.source

    // signal declarations
    signal clicked                                          

    // javascript functions
    function doSomething(x) {
        return x + photoImage.width
    }

    // object properties
    color: "gray"
    // try to group related properties together
    x: 20
    y: 20
    height: 150
    // large bindings
    width: {
        // Always space all operators, and add a single space
        // before opening brackets.
        if (photoImage.width > 200) {
            photoImage.width
        } else {
            height
        }
    }

    // child objects (contentItem, defaults, etc. first)
    Rectangle {
        id: border
        anchors.centerIn: parent
        color: "white"

        Image {
            id: photoImage
            anchors.centerIn: parent
        }
    }
    
    Text {
        anchors.centerIn: photoImage
        // Use the ellipsis character (…) for manual ellision within
        // UI text and don't translate untranslatables.
        text: qsTr("an emoji: %1 and a number: %2 …").arg("💡").arg(42)
    }

    // states
    states: State {
        name: "selected"
        PropertyChanges {
            target: border
            color: "red"
        }
    }

    // transitions
    transitions: Transition {
        from: ""
        to: "selected"
        ColorAnimation {
            target: border
            duration: 200
        }
    }
}
```