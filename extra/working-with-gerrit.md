# Working With Gerrit

## Account Setup

+ Gerrit server: https://review.jami.net
+ User documentation: https://review.jami.net/Documentation/intro-user.html
+ Jami projects on Gerrit: https://review.jami.net/#/admin/projects/

1. Sign-in with your google or github account or git.jami.net account
2. You'll also need to [upload an SSH key](https://review.jami.net/settings/#SSHKeys) to be able to commit changes for review.
3. Don't forget to select a username.
4. Finally, the email address specified in your git config must match one the email address registered with your Gerrit account.

*Note for Savoir-faire Linux employees: please continue to use your @savoirfairelinux.com email address.*

## To Know your Git Config

`git config --list`

## To Test your SSH Access

To check that your SSH access is properly setup, run the following command:

`ssh -p 29420 <username>@review.jami.net`

<username> is your Gerrit username, that you should have set during the account creation. If not, you can do that here.

```
If your access is granted, you should see a message like:

****    Welcome to Gerrit Code Review    ****

Hi, you have successfully connected over SSH.

  Unfortunately, interactive shells are disabled.
  To clone a hosted Git repository, use:

  git clone ssh://<username>@review.jami.net:29420/REPOSITORY_NAME.git

Connection to review.jami.net closed.
```

## Git Configuration

Gerrit is the official git repository.

## To update the configuration

You must update your remote information to use now the Gerrit repository. To do that, update your origin url:

`git remote set-url origin ssh://<username>@review.jami.net:29420/<project_name>`

Replace `<project_name>` by the correct project (example: ring-daemon)

Or clone the existing repository if you want to start fresh.

## To Push by Default in refs/for/master

You can configure git to automatically create a review when a change is pushed.

`git config remote.origin.push HEAD:refs/for/master`

## To Create the Review

When pushing to this magic branch, a review will automatically be created on Gerrit.

`git push origin HEAD:refs/for/master`

If you configured the default to refs/for/master as described above, simply

`git push`

If HEAD currently points to the branch with the commits you'd like to push. Ideally, you should work in a feature/bug branch for the issue at hand. Then you can do:

`git push origin <bugfix_branchname>:refs/for/master`

If this is the first time you've pushed, you will be prompted to install a post-commit Hook to insert a Change-ID in your commit message. Gerrit needs this to track patchsets and will reject pushes until you install it. Simply copy paste the command to install the hook as instructed by Gerrit, and amend your commits.

## To Push a Private patch

You can push a work in progress (a.k.a draft) by pushing to refs/for/master%private

For instance, you may want a "private" remote to push to; open <project_dir>/.git/config and add:

```
[remote "private"]

	url = ssh://<username>@review.jami.net:29420/ring-daemon
	push = HEAD:refs/for/master%private
```

Then:

`git push private`

Private work the same way as patchsets, except they are not visible to others by default and don't trigger any Jenkins builds. A draft can then be shared or published.
