# LRC documentation

NOTE: Had to delete a lot of image links that were broken, so this
page is pretty useless right now.

*This page is a stub.*

### Introduction

-   Lrc (Libringclient) is an interface between the clients and
	the daemon. It ensures to get the same behaviour for all client
	using it.

note: red = missing feature.

### DatabaseManager

-   this class is an interface between lrc and the sqlite database. This
	class should not be called directly from the client.

### NewCallModel class and NewCall namespace

-   NewCallModel is an interface used to manage the calls.


-   When we need information about calls, members functions from the
	model can return NewCall::Info.


-   When we need to perform some operation on a call, we pass it callId
	to the delegate performing the operation.


-   note about the name : we are using New as prefix to avoid conflict
	with the current CallModel and Call objects.

### ContactModel class and Contact namespace

-   ContactModel is an interface to manage the contacts.

-   When we need information about contact(s), members functions from
	the model can return Contact::Info

-   When we need to perform some operation on a contact, we pass it uri
	to the delegate performing the operation.

### ConversationModel class, Conversation namespace and Message namespace

-   ConversationModel is an interface used to manage conversations
	and messages.

-   When we need information about some conversation(s), members
	functions from the model can return Conversation::Info.
	Conversation::Info contains Messages.

-   note about Message : so far, we didn't need a MessageModel, but this
	could be come soon.
