#######################
Basic Features
#######################

.. toctree::
   :maxdepth: 1

   manage-accounts
   manage-contacts
   swarm
   let's-do-a-call
   file-transfer
