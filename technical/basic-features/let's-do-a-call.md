# Calling

**NOTE: this page detail the principle for Jami accounts. For SIP accounts, the SIP protocol is used.**

## Daemon side

When creating a call between two peers, Jami mainly uses known protocols such as ICE, SIP or TLS. However, to make it distributed, the process of creating a call is a bit different. To summarize, when someone wants to contact one of its contact, this is what they will do:

1. Search the contact presence on the DHT (for more details, see https://git.jami.net/savoirfairelinux/ring-project/wikis/technical/2.2.%20Manage%20contacts)
2. Once the contact is found, send a call request, announcing the known candidates (the ip of each network interfaces + relay addresses (TURN) + reflexives addresses (UPnP, public ones).
3. Wait for the response of the contact (they will respond their known addresses).
4. Negotiate the socket via ICE. In fact, two ICE sessions are negotiated. One (preferred) in TCP, one in UDP (as a fallback).
5. Then, the socket is encrypted in TLS (if TCP) or DTLS (if UDP).
6. The contact is now able to accept or decline the call. When they accept, a ICE transport (UDP only for now) is negotiated to create 4 new sockets for the medias (2 for audio, 2 for video).
7. The call is now alive!

### Exchange ICE candidates

Everything really starts in `jamiaccount.cpp` (`JamiAccount::startOutgoingCall`). Once both ICE objects are ready and when the contact is found via the DHT, the call request for the contact is crafted. This request contains all the informations necessary for the remote ICE session defined by:

```cpp
dht::IceCandidates(callvid,  blob)
```

where `callvid` is a random number used to identify the call and blob contains two concatened ICE messages (`IceTransport::packIceMsg` in `ice_transport.cpp`) containing the password of the session, the *ufrag* and ICE candidates.) like:

```
0d04b935
7c33834e7cf944bf0e367b42
H6e6ca382 1 UDP 2130706431 2607:fad8:4:6:9eb6:d0ff:dead:c0de 14133 typ host
H42c1g477 1 UDP 2130706431 fe80::9eb6:d0ff:fee7:1412 14133 typ host
Hc0a8027e 1 UDP 2130706431 192.168.0.123 34567 typ host
Sc0a8027e 1 UDP 1694498815 X.X.X.X 32589 typ srflx
0d04b932
7c33834e7cf944bf0e367b47
H6e6ca682 1 TCP 2130706431 2607:fad8:4:6:9eb6:d0ff:dead:c0de 50693 typ host tcptype passive
H6e6ca682 1 TCP 2130706431 2607:fad8:4:6:9eb6:d0ff:dead:c0de 9 typ host tcptype active
H42c1b577 1 TCP 2130706431 fe80::9eb6:d0ff:fee7:1412 50693 typ host tcptype passive
H42c1b577 1 TCP 2130706431 fe80::9eb6:d0ff:fee7:1412 9 typ host tcptype active
Hc0a8007e 1 TCP 2130706431 192.168.0.123 42751 typ host tcptype passive
Hc0a8007e 1 TCP 2130706431 192.168.0.123 9 typ host tcptype active
Sc0a8007e 1 TCP 1694498815 X.X.X.X 42751 typ srflx tcptype passive
```

and is sent via the DHT in an encrypted message for the device to `hash(callto:xxxxxx)` where `xxxxxx` is the device id. The peer will answer at the exact same place (but encrypted for the sender device) its own `dht::IceCandidates`. See `JamiAccount::replyToIncomingIceMsg` for more details.

The ICE session is created both side when they have all the candidates (so for the sender, when the reply from the contact is received).

### ICE negotiation

Pending calls are managed by `JamiAccount::handlePendingCallList()`, which first wait that the TCP negotiation finish (and if it fails, wait for the UDP one). The code for the ICE negotiation is mainly managed by [pjproject](https://github.com/pjsip/pjproject) but for Jami, the interesting part is located in `ice_transport.cpp`. Moreover, we add some important patches/features on top of *pjproject* not merged upstream for now (for example, ICE over TCP). These patches are present in `contrib/src/pjproject`.

### Encrypt the control socket

Once the socket is created and managed by an **IceTransport** instance, it is then wrapped in a **SipTransport** corresponding to a *TlsIceTransport*. The main code is located into `JamiAccount::handlePendingCall()` and the wrapping is done into `SipTransportBroker::getTlsIceTransport`. Finally, our session is managed by **TlsSession** in `daemon/src/security/tls_session.cpp` and uses the GnuTLS library.

So, the control socket will be a TLS (1.3 if your and your peer gnutls version support it) if a TCP socket is negotiated. If a UDP socket is negotiated instead (due to firewall restrictions/problem in the negotiation/etc), the socket will use DTLS (still managed by the same parts).

The control socket is used to transmit SIP packets, like invites, custom messages (Jami sends the VCard of your profile on this socket at the start of the call, or the rotation of the camera), text messages.

Related articles:

+ https://jami.net/improved-video-rotation-support/
+ https://jami.net/peer-to-peer-file-sharing-support-in-jami/

### Media sockets

Media sockets are SRTP sockets where the key is negotiated through the TLS Session previously created.
**TODO**

### Architecture



## Client APIs

## LRC
