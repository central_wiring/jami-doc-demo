# Understanding Swarms

## Synospis

The goal of this document is to describe how group chats
(a.k.a. **swarm chat**) will be implemented in Jami.

A *swarm* is a group able to discuss without any central authority in
a resilient way. Indeed, if two person doesn't have any connectivity
with the rest of the group (ie Internet outage) but they can contact
each other (in a LAN for example or in a sub network), they will be
able to send messages to each other and then, will be able to sync
with the rest of the group when it's possible.

So, the *swarm* is defined by:
1. Ability to split and merge following the connectivity.
2. Syncing of the history. Anyone must be able to send a message to the whole group.
3. No central authority. Can't rely on any server.
4. Non-repudiation. Devices must be able to verify old messages validity and to replay the whole history.
5. PFS on the transport. Storage is managed by the device.

Main idea is to get a synchronized merkle tree with the participants.

We identified four modes for swarm chat that we want to implement:
+ **ONE_TO_ONE**, basically the case we have today, when you discuss to a friend
+ **ADMIN_INVITES_ONLY** generally a class where the teacher can invite people, but not students
+ **INVITES_ONLY** a private group of friends
+ **PUBLIC** basically an opened forum

## Scenarios

### Create a Swarm

*Bob wants to create a new swarm*

1. Bob create a local git repository.
2. Then, he adds an initial signed commit and adds the following:
	+ His public key in `/admins`
	+ His device certificate in `/devices`
	+ His CRL in `/crls`
3. The hash of the first commit becomes the **ID** of the conversation
4. Bob announce to his other devices that he creates a new conversation. This is done via an invite to join the swarm sent through the DHT to other devices linked to that account.

### Adding someone

*Alice adds Bob*

1. Alice adds Bob to the repo:
	+ Adds the invited uri in `/invited`
	+ Adds the CRL into `/crls`
2. Alice sends a request on the DHT

### Receiving an invite

*Alice gets the invite to join the previously create swarm*

1. She accepts the invite (if decline, do nothing, it will just stay into invited and Alice will never receives any message)
2. A peer to peer connection between Alice and Bob is done.
3. Alice pull the git repo of Bob. **WARNING this means that messages needs a connection, not from the DHT like today**
4. Alice validates commits from Bob
5. To validate that Alice is a member, she removes the invite from `/invited` directory, then adds her certificate into the `/members` directory
6. Once all commits validated and on her device, other members of the group are discovered by Alice. with these peers, she will construct the **DRT** (explained below) with Bob as a bootstrap.

### Sending a message

*Alice sends a message*

Sending a message is pretty simple. Alice write a commit-message with the following format:

**TODO format unclear**

and adds her device and CRL to the repository if missing (others must be able to verify the commit). Merge conflicts are avoided because we are mostly based on commit messages, not files (unless CRLS + certificates but they are located). then she announce the new commit via the **DRT** with a service message (explained later) and ping the DHT for mobile devices (they must receive a push notification).

For pinging other devices, the sender sends to other members a SIP messages with mimetype = "application/im-gitmessage-id" containing a JSON with the "deviceId" which sends the message, the "id" of the conversation related and the "commit"

### Receiving a message

*Bob receives the message from Alice*

1. *Bob* do a git pull on *Alice*
2. Commits MUST be verified via a hook
3. If all commits are valid, commits are stored and displayed. Then *Bob* announce the message via the DRT for other devices.
4. If all commits are not valid, pull is cancelled. *Alice* must restablish her state to a correct state. **TODO process*

### Validating a commit

To avoid users to push some unwanted commits (with conflicts, false messages, etc), this is how each commits (from the oldest to the newest one) MUST be validated before merging a remote branch:

Note: if the validation fails, the fetch is ignored and we do not merge the branch (and remove the datas), and the user should be notified
Note2: If a fetch is too big, it's not done (**TODO**)

+ For each commits, check that the device who try to send the commit is authorized at this moment, and that the certificates are present (in /devices for the device, and in /members or /admins for the issuer).
+ 3 cases. The commit has 2 parents, so it's a merge, nothing more to validate here
+ The commit has 0 parent, it's the initial commit:
	+ Check that admin cert is added
	+ Check that device cert is added
	+ Check CRLs added
	+ Check that no other file is added
+ The commit has 1 parent, commit msg is a JSON with a type:
	+ If text (or other mimetype that doesn't change files)
		+ Check signature from certif in repo
		+ Check that no weird file is added outside device cert nor removed
	+ If vote
		+ Check that vote is for user that signs the commit
		+ Check that vote is from an admin and device present & not banned
		+ Check that no weird file is added nor removed
	+ If member
		+ If adds
			+ Check that commit is correctly signed
			+ Check that certificate is added in /invited
			+ Check that no weird file is added nor removed
			+ If ONE_TO_ONE, check that we only have one admin, one member
			+ If ADMIN_INVITES_ONLY, check that invite is from an admin
		+ If joins
			+ Check that commit is correctly signed
			+ Check that device is added
			+ Check that invitation is moved to members
			+ Check that no weird file is added nor removed
		+ If kickban
			+ Check that vote is valid
			+ Check that the user is ban via an admin
			+ Check that member or device certificate is moved to banned/
			+ Check that only files related to the vote is removed
			+ Check that no weird file is added nor removed
	+ else fail. Notify the user that they may be with an old version or that peer tried to submit unwanted commits


### Ban a device

*Alice, Bob, Carla, Denys are in a swarm. Alice bans Denys*

This is one of the most difficult scenario in our context. Without central authority we can't trust:

1. Timestamps of generated commits
2. Conflicts with banned devices. If multiple admin devices are present and if Alice can speak with Bob but not Denys and Carla ; Carla can speak with Denys ; Denys bans Alice, Alice bans Denys, what will be the state when the 4 members will merge the conversations.
3. A device can be compromised, stolen or its certificate can expire. We should be able to ban a device and avoid that it lies about its expiration or send messages in the past (by changing its certificate or the timestamp of its commit).

Similar systems (with distributed group systems) are not so much, but this is some examples:

+ [mpOTR doesn't define how to ban someone](https://www.cypherpunks.ca/~iang/pubs/mpotr.pdf)
+ Signal, without any central server for group chat (EDIT: they recently change that point) doesn't give the ability to ban someone of a group.

This voting system need a human action to ban someone or must be based on the CRLs infos from the repository (because we can't trust external CRLs)

### Remove a device from a conversation

This is the only part that MUST have a consensus to avoid conversation's split, like if two members kick each others from the conversation, what will see the third one?

This is needed to detect revoked devices, or simply avoid to get unwanted people present in a public room. The process is pretty similar between a member and a device:

*Alice removes Bob*

Note: Alice MUST be admins to vote

+ First, she votes for ban Bob. To do that, she creates the file in /votes/members/uri_bob/uri_alice (members can be replaced by devices for a device) and commit
+ Then she checks if the vote is resolved. This means that >50% of the admins agree to ban Bob (if she is alone, it's sure it's more than 50%).
+ If the vote is resolved, files into /votes can be removed, all files for Bob in /members, /admins, /CRLs, /devices can be removed (or only in /devices if it's a device that is banned) and Bob's certificate can be placed into /banned/members/bob_uri.crt (or /banned/devices/uri.crt if a device is banned) and committed to the repo
+ Then, Alice informs other users (outside Bob)

### Remove a conversation

1. Save in convInfos removed=time::now() (like removeContact saves in contacts) that the conversation is removed and sync with other user's devices
2. Now, if a new commit is received for this conversation it's ignored
3. Now, if Jami startup and the repo is still present, the conversation is not announced to clients
4. Two cases:
	a. If no other member in the conversation we can immediately removes the repository
	b. If still other members, commit that we leave the conversation, and now wait that at least another device sync this message. This avoid the fact that other members will still detect the user as a valid member and still sends new messages notifications.
5. When we are sure that someone is synched, remove erased=time::now() and sync with other user's devices
6. All devices owned by the user can now erase the repository and related files

## How to specify a mode

Modes can't be changed through the time. Or it's another conversation. So, this data is stored into the initial commit message.
The commit message will be the following:


```json
{
	"type": "initial",
	"mode": 0,
}
```

For now, "mode" accepts values 0 (ONE_TO_ONE), 1 (ADMIN_INVITES_ONLY), 2 (INVITES_ONLY), 3 (PUBLIC)

### Processus for 1:1 swarms

The goal there is to keep the old API (addContact/removeContact, sendTrustRequest/acceptTrustRequest/discardTrustRequest) to generate swarm with a peer and its contact. This still imply some changes that we cannot ignore:

The process is still the same, an account can add a contact via addContact, then send a TrustRequest via the DHT. But two changes are necessary:
1. The TrustRequest embeds a "conversationId" to inform the peer what conversation to clone when accepting the request
2. TrustRequest are retried when contact come backs online. It's not the case today (as we don't want to generate a new TrustRequest if the peer discard the first). So, if an account receives a trust request, it will be automatically ignored if the request with related conversation is declined (as convRequests are synched)

Then, when a contact accepts the request, a period of sync is necessary, because the contact now needs to clone the conversation.

removeContact() will remove the contact and related 1:1 conversations (with the same processus as "Remove a conversation"). The only note there is that if we ban a contact, we don't wait for sync, we just remove all related files.

#### Tricky scenarios

There is some cases were two conversations can be created. This is at least two of those scenarios:

1. Alice adds Bob
2. Bob accepts
3. Alice removes Bob
4. Alice adds Bob

or

1, Alice adds Bob & Bob adds Alice at the same time, but both are not connected together

In this case, two conversations are generated. We don't want to removes messages from users or choose one conversation here. So, sometimes two 1:1 swarm between the same members will be shown. It will generate some bugs during the transition time (as we don't want to break API, the infered conversation will be one of the two shown conversation, but for now it's "ok-ish", will be fixed when clients will fully handle conversationId for all APIs (calls, file transfer, etc)).

### Conversations requests specification

Conversations requests are represented by a **Map<String, String>** with the following keys:

+ id: the conversation id
+ from: uri of the sender
+ received: timestamp
+ title: (optional) name for the conversation
+ description: (optional)
+ avatar: (optional)

### Conversation's profile synchronization

To be identifiable, a conversation generally needs some metadatas, like a title (eg: Jami), a description (eg: some links, what is the project, etc), and an image (the logo of the project). Those metadatas are optional, but shared across all members, so need to be synced and incorporated in the requests.

#### Storage in the repository

The profile of the conversation is stored in a classic vCard file at the root (`/profile.vcf`) like:

```
BEGIN:VCARD
VERSION:2.1
FN:TITLE
DESCRIPTION:DESC
END:VCARD
```

#### Synchronization

To update the vCard, an user with enough permissions (by default: =ADMIN) needs to edit `/profile.vcf`. and will commit the file with the mimetype `application/update-profile`. The new message is sent via the same mechanism and all peers will receive the **MessageReceived** signal from the daemon. The branch is dropped if the commit contains other files or too big or if done by a non authorized member (by default: <ADMIN).

#### Merge conflicts management

Because two admins can change the description at the same time, a merge conflict can occurs on `profile.vcf`. In this case, the commit with the higher hash (eg ffffff > 000000) will be chosen.

#### APIs

The user got 2 methods to get and set conversation's metadatas:

```xml
	   <method name="updateConversationInfos" tp:name-for-bindings="updateConversationInfos">
		   <tp:added version="10.0.0"/>
		   <tp:docstring>
			   Update conversation's infos (supported keys: title, description, avatar)
		   </tp:docstring>
		   <arg type="s" name="accountId" direction="in"/>
		   <arg type="s" name="conversationId" direction="in"/>
		   <annotation name="org.qtproject.QtDBus.QtTypeName.In2" value="VectorMapStringString"/>
		   <arg type="a{ss}" name="infos" direction="in"/>
	   </method>

	   <method name="conversationInfos" tp:name-for-bindings="conversationInfos">
		   <tp:added version="10.0.0"/>
		   <tp:docstring>
			   Get conversation's infos (mode, title, description, avatar)
		   </tp:docstring>
		   <annotation name="org.qtproject.QtDBus.QtTypeName.Out0" value="VectorMapStringString"/>
		   <arg type="a{ss}" name="infos" direction="out"/>
		   <arg type="s" name="accountId" direction="in"/>
		   <arg type="s" name="conversationId" direction="in"/>
	   </method>
```

where `infos` is a `map<str, str>` with the following keys:

+ mode: READ-ONLY
+ title
+ description
+ avatar

#### Re-import an account (link/export)

The archive MUST contains conversationId to be able to be able to retrieve conversations on new commits after a re-import (because there is no invite at this point). If a commit come for a conversation not present there is two possibilities:

+ The conversationId is there, in this case the daemon is able to re-clone this conversation
+ The conversationId is missing, so the daemon asks (via a message `{{"application/invite", conversationId}}`) a new invite that the user needs to (re)accepts

Note, a conversation can only be retrieven if a contact or another device is there, else it will be lost. There is no magic.

### Other mime types

+ `application/call-history+json` with a JSON containing the `id` and the `duration` of the call
+ `application/data-transfer+json` with a JSON containing the `tid` of the file

## Used protocols

### Git

#### Why this choice

Each conversation will be a git repository. This choice is motivated by:

1. We need to sync and ordering messages. The Merkle Tree is the perfect structure to do that and can be linearized by merging branches. Moreover, because it's massively used by Git, it's easy to sync between devices.
2. Distributed by nature. Massively used. Lot of backends and plugguable.
3. Can verify commits via hooks and massively used crypto
4. Can be stored in a database if necessary
5. Conflicts are avoided by using commit messages, not files.

#### What we have to validate

+ Performance? `git.lock` can be low
+ Hooks in libgit2
+ Multiple pulls at the same time?

#### Limits

History can't be deleted. To delete a conversation, the device has to leave the conversation and create another one.

However, non permanent messages (like messages readable only for some minutes) can be sent via a  special message via the DRT (like Typing or Read notifications).

Moreover editing messages will be possible! (`commit --fixup`)

#### Structure

```
/
 - invited
 - admins (public keys)
 - members (public keys)
 - devices (certificates of authors to verify commits)
 - banned
   - devices
   - members
 - votes
	- members
		- uri
	- devices
		- uri
 - CRLs
```

#### Attacks?

+ Avoid git bombs

#### Notes

Timestamp of a commit can be trusted because it's editable. Only the user's timestamp can be trusted.

### TLS

Git operations, control messages, files and other things will use a p2p TLS v1.3 link with only cyphers which garanty PFS. So each key is renegotiated for each new connexion.

### DHT (udp)

Used to send messages for mobiles (to trigger push notifications) and to initiate TCP connexions.

### DRT (name will change)

The DRT is a new concept used in swarm to maintain p2p connections. Indeed, group members define a graph of nodes (identified by a hash) en must be connected.

So we need a structure that:

1. Maximize the connected nodes at every time
2. Minimize the time for message transmission
3. Minimize links between peers
4. Needs low calculation

Several solutions exists:

1. Each node has a connection  to the next node. So we only need $N$ connections, but it's not effective to transmit a message, because the message will go though all peers, one by one.
2. Each nodes is connected to all other nodes $N\timesN$ connections. Effective to transmit, but need more resources **WILL BE CHOSEN FOR THE FIRST VERSION**
3. *Maximizing the Coverage of Roadmap Graph for Optimal Motion Planning*
https://www.hindawi.com/journals/complexity/2018/9104720/. But need calculations
4. Use the algorithm of the DHT for the routing table. The 4 points are basically solved and already used by Jami in UDP.

Note: to optimize the socket numbers, a socket will be given by a **ConnectionManager** to get multiplexed sockets with a given hash. This means that if we need to transmit several files, and chat with someone, only one socket will be used.

### File transfer (libtorrent?)

**TODO**

### Network activity

#### Process to invite someone

Alice wants to invite Bob:

1. Alice adds bob to a conversation
2. Alice generates an invite: { "application/invite+json" : {
	"conversationId": "$id",
	"members": [{...}]
}}
3. Two possibilites for sending the message
	a. If not connected, via the DHT
	b. Else, Alice sends on the SIP channel
4. Two possibilities for Bob
	a. Receives the invite, a signal is emitted for the client
	b. Not connected, so will never receives the request cause Alice must not know if Bob just ignored or blocked alice. The only way is to regenerate a new invite via a new message (cf next scenario)

#### Process to send a message to someone

Alice wants to send a message to Bob:

1. Alice adds a message in the repo, giving an ID
2. Alice gets a message received (from herself) if successful
3. Two possibilities, alice and bob are connected, or not. In both case a message is crafted: { "application/im-gitmessage-id" : "{"id":"$convId", "commit":"$commitId", "deviceId": "$alice_device_hash"}"}.
	a. If not connected, via the DHT
	b. Else, Alice sends on the SIP channel
4. Four possibilities for Bob:
	a. Bob is not connected to Alice, so if he trusts alice, ask for a new conection and go to b.
	b. If connected, fetch from alice and announce new messages
	c. Bob doesn't know that conversation. Ask through the DHT to get an invite first to be able to accept that conversation ({"application/invite", conversationId})
	d. Bob is disconnected (no network, or just closed). He will not receive the new message, but will try to sync when the next connection will occurs


### Implementation

**!! OLD DRAFT !!**

Note: Following notes are not organized yet. Just some line of thoughts.

## Crypto improvements.

For a serious group chat feature, we also need serious crypto. With the current design, if a certificate is stolen as the previous DHT values of a conversation, the conversation can be decrypted. Maybe we need to go to something like **Double ratchet**.

Note: a lib might exists to implement group conversations. TODO, investigate.

Needs ECC support in OpenDHT

## Usage

### Add Roles?

There is two major use case for group chats:

1. Something like a Mattermost in a company, with private channels, and some roles (admin/spectator/bot/etc) or for educations (where only a few are active).
2. Horizontal conversations like a conversation between friends.

Ring will be for which one?

#### Implementation idea

A certificate for a group which sign user with a flag for a role. Adding or revoking can also be done.

### Join a conversation

+ Only via a direct invite
+ Via a link/QR Code/whatever
+ Via a room name? (a **hash** on the DHT)

## What we need

+ Confidentiality: members outside of the group chat should not be able to read messages in the group
+ Forward secrecy: if any key from the group os compromised, previous messages should remain confidential (as much as possible)

+ Message ordering: There is a need to have messages in the right order
+ Synchronization: There is also a need to be sure to have all messages at soon as possible.
+ Persistence: Actually, a message on the DHT lives only 10 minutes. Because it's the best timing calculated for this kind of DHT. To persist datas, the node must re-put the value on the DHT every 10 minutes. Another way to do when the node is offline is to let nodes re-put the data. But, if after 10 minutes, 8 nodes are still here, they will do 64 requests (and it's exponential). The current way to avoid spamming for that is queries. This will still do 64 requests but limit the max redundency to 8 nodes.

## Other distributed ways

+ IPFS: Need some investigation
+ BitMessage: Need some investigation
+ Maidsafe: Need some investigation

### Based on current work we have

Groups chat can be based on the same work we already have for multi devices (but here, with a group certificate). Problems to solve:

1. History sync. This need to move the database from the client into the daemon.
2. If nobody is connected, the synchronization can't be done, and the person will never see the conversation

### Another dedicated DHT

Like a DHT with super user. (Not convinced)

## File transfer

Currently the file transfer algorithm is based on a TURN connection (See https://git.ring.cx/savoirfairelinux/ring-project/wikis/tutorials/File-transfer). In case of a big group, this will be bad. We first need a p2p implem for the file transfer. Implement the RFC for p2p transfer.

Other problem: currently there is no implementation for TCP support for ICE in PJSIP. This is mandatory for this point (in pjsip or home made)

## Resources

+ https://eprint.iacr.org/2017/666.pdf
+ Robust distributed synchronization of networked linear systems with intermittent information (Sean Phillips and Ricardo G.Sanfelice)
