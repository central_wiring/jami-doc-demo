# Managing Contacts

This section will present how to find and add a contact from the DHT to the client. The usage of a name server will not be explained here. If you want details about that, please read: https://git.ring.cx/savoirfairelinux/ring-project/wikis/technical/Name-Server-Protocol

## Presence on the network

### Announce the presence on the DHT

The presence is pretty simple to announce on the DHT. In fact, it's
just a value containing the device hash (see [previous
section](https://git.ring.cx/savoirfairelinux/ring-project/wikis/technical/2.1.-Manage-Accounts))
on the hash corresponding to the Ring ID. So, if we have the account
`bf5f1e21d3eb2c1246946aa49d5dcf3e5b9cb1b9` with the device
`62fbdff0ce86f368c7d3c2682539e5ba9e06404f`, the following defined
value will be sent over the DHT:

```cpp
/**
 * Device announcement stored on DHT.
 */
struct RingAccount::DeviceAnnouncement : public dht::SignedValue<DeviceAnnouncement>
{
private:
	using BaseClass = dht::SignedValue<DeviceAnnouncement>;
public:
	static const constexpr dht::ValueType& TYPE = dht::ValueType::USER_DATA;
	dht::InfoHash dev;
	MSGPACK_DEFINE_MAP(dev);
};
```

(This value can be put with `dht_.put(h, VALUE, dht::DoneCallback{}, {}, true);`, as a permanent put). If the device is announced, the device is present. For now, there is no way to delete or edit a value on the DHT (this will come when OpenDHT will supports ECC). So, the presence always have a delay for now (mean delay: expire-time/2, so 2min30 for now).

### Get if a contact is present

Now our presence on the network, it's time to get if somebody is present on the DHT. With the previous section, it's easy to do the reverse process. To know if somebody is present on the DHT (ex: `bf5f1e21d3eb2c1246946aa49d5dcf3e5b9cb1b9`), we have to get value at `bf5f1e21d3eb2c1246946aa49d5dcf3e5b9cb1b9` and retrieve the `DeviceAnnouncement` on this hash. The related code in the ring daemon is in `ringaccount.cpp`:

```cpp
auto shared = std::static_pointer_cast<RingAccount>(shared_from_this());
auto treatedDevices = std::make_shared<std::set<dht::InfoHash>>();
dht_.get<dht::crypto::RevocationList>(to, [to](dht::crypto::RevocationList&& crl){
	tls::CertificateStore::instance().pinRevocationList(to.toString(), std::move(crl));
	return true;
});
dht_.get<DeviceAnnouncement>(to, [shared,to,treatedDevices,op](DeviceAnnouncement&& dev) {
	if (dev.from != to)
		return true;
	if (treatedDevices->emplace(dev.dev).second)
		op(shared, dev.dev);
	return true;
}, [=](bool /*ok*/){
	{
		std::lock_guard<std::recursive_mutex> lock(shared->buddyInfoMtx);
		auto buddy_info_it = shared->trackedBuddies_.find(to);
		if (buddy_info_it != shared->trackedBuddies_.end()) {
			if (not treatedDevices->empty()) {
				for (auto& device_id : *treatedDevices)
					shared->onTrackedBuddyOnline(buddy_info_it, device_id);
			} else
				shared->onTrackedBuddyOffline(buddy_info_it);
		}
	}
	RING_DBG("[Account %s] found %lu devices for %s",
				getAccountID().c_str(), treatedDevices->size(), to.to_c_str());
	if (end) end(shared, not treatedDevices->empty());
});
```

And that's all.

### Pending request

### Send a request

**TODO craft request**

Finally, once the trust request is crafted, we can push the request to the following hash: `InfoHash("inbox:" + deviceId)`

The following code is used in the daemon:
```cpp
dht_.putEncrypted(dht::InfoHash::get("inbox:"+dev.toString()), dev, dht::TrustRequest(DHT_TYPE_NS, payload));
```

### Receiving a request

**TODO**

(Accept/Block/Discard)

### Daemon API

All methods to follow the presence of a buddy is located in the `PresenceManager` such as:

```xml
<signal name="newBuddyNotification" tp:name-for-bindings="newBuddyNotification">
	<tp:added version="1.3.0"/>
	<tp:docstring>
		Notify when a registered presence uri presence informations changes
	</tp:docstring>
	<arg type="s" name="accountID">
		<tp:docstring>
			The associated account
		</tp:docstring>
	</arg>
	<arg type="s" name="buddyUri">
		<tp:docstring>
			The registered URI
		</tp:docstring>
	</arg>
	<arg type="b" name="status">
		<tp:docstring>
			Is the URI present or not
		</tp:docstring>
	</arg>
	<arg type="s" name="lineStatus">
		<tp:docstring>
			A string containing informations from the user (human readable)
		</tp:docstring>
	</arg>
</signal>
```

All methods and signals used to manage trust requests and contacts are in the `ConfigurationManager` such as:

```xml
<method name="getTrustRequests" tp:name-for-bindings="getTrustRequests">
	<tp:added version="2.2.0"/>
	<arg type="s" name="accountID" direction="in">
	</arg>
	<annotation name="org.qtproject.QtDBus.QtTypeName.Out0" value="VectorMapStringString"/>
	<arg type="aa{ss}" name="requests" direction="out" >
		<tp:docstring>
			A list of contact request details. Details:
			- from: account ID of sender
			- received: UNIX timestamp of reception date
			- payload: attached payload
		</tp:docstring>
	</arg>
</method>

<method name="acceptTrustRequest" tp:name-for-bindings="acceptTrustRequest">
	<tp:added version="2.2.0"/>
	<arg type="s" name="accountID" direction="in">
	</arg>
	<arg type="s" name="from" direction="in">
	</arg>
	<arg type="b" name="success" direction="out" tp:type="Boolean">
		<tp:docstring>
			True if the operation succeeded.
		</tp:docstring>
	</arg>
</method>

<method name="discardTrustRequest" tp:name-for-bindings="discardTrustRequest">
	<tp:added version="2.2.0"/>
	<arg type="s" name="accountID" direction="in">
	</arg>
	<arg type="s" name="from" direction="in">
	</arg>
	<arg type="b" name="success" direction="out" tp:type="Boolean">
		<tp:docstring>
			True if the operation succeeded.
		</tp:docstring>
	</arg>
</method>

<signal name="incomingTrustRequest" tp:name-for-bindings="incomingTrustRequest">
	<tp:added version="2.2.0"/>
	<tp:docstring>
		Notify clients that a new contact request has been received.
	</tp:docstring>
	<arg type="s" name="accountID">
	</arg>
	<arg type="s" name="from">
	</arg>
	<arg type="ay" name="payload">
	</arg>
	<arg type="t" name="receiveTime">
	</arg>
</signal>

<method name="sendTrustRequest" tp:name-for-bindings="sendTrustRequest">
	<tp:added version="2.2.0"/>
	<arg type="s" name="accountID" direction="in">
	</arg>
	<arg type="s" name="to" direction="in">
	</arg>
	<arg type="ay" name="payload" direction="in">
	</arg>
</method>

<method name="addContact" tp:name-for-bindings="addContact">
	<tp:added version="3.0.0"/>
	<arg type="s" name="accountID" direction="in">
	</arg>
	<arg type="s" name="uri" direction="in">
	</arg>
</method>

<method name="removeContact" tp:name-for-bindings="removeContact">
	<tp:added version="3.0.0"/>
	<arg type="s" name="accountID" direction="in">
	</arg>
	<arg type="s" name="uri" direction="in">
	</arg>
	<arg type="b" name="ban" direction="in" tp:type="Boolean">
		<tp:docstring>
			True if the the contact should be banned.
			If false, the contact is removed from the contact list (banned or not).
		</tp:docstring>
	</arg>
</method>
```

If you want some examples, these methods are used into `contactmodel.cpp` in LRC.
