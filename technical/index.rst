####################
Technical Reference
####################

This chapter includes in-depth explanations of how Jami is
designed. It also serves as a reference for contributors.

.. toctree::
   :maxdepth: 2

   identifiers
   APIs
   basic-features/index
   advanced-features/index
   certificates
   name-server-protocol
   choosing-crf-value-for-encoder


If you're reading this, you probably want to either contribute to one
of the projects or to implement your own client. There are three main
layers in this project:

1. `OpenDHT <https://opendht.net>`_ for p2p communication. You can
   interact with this library like any Cpp library or with the python
   wrapper or via the `REST
   API <https://github.com/savoirfairelinux/opendht/wiki/REST-API>`_.
2. The Jami daemon, which is the main part. It's the part which does
   all the logic for Ring and interacts with OpenDHT, pjsip, ffmpeg
   and all libraries used and implement the whole protocol. If you
   want to do a client, we recommend that you implement your client on
   top of this daemon and use one of the many APIs
   (REST/dbus/libwrap/JNI).
3. The client part, which is basically the frontend.
