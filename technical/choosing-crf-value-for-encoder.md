# Choosing CRF value for encoder

## Context
Bandwidth usage by the Jami application is not optimal for all types of connections. Indeed, in some cases, the user experience is not good (satellite connection, ...) despite the bandwidth management algorithm.

## Observation
It is not necessary to aim for an optimal quality (CRF < 20) because beyond that, the visual perception is almost similar while the data flow (bitrate) required is much higher.

## Objective
The purpose of this document is to verify the impact of a change in video quality with the CRF parameter of the encoder.

## Test
These tests were performed by comparing :
- The first one encoded with Jami's current parameters
- The second encoded with a lower quality

Each of these tests were performed for the following resolutions: 1080p, 720p and 436p.

For each of these resolutions several bitrates have been used:
- 300 Kbit/s (Jami low value)
- 1.5 Mbit/s (Intermediate value)
- 6 Mbit/s (High value)

The graphs show the evolution of the bitrate with the file being tested (resolution and specific set bitrate).

A visual comparison (side by side) was made for each test.

Thanks to this test we can estimate the bitrate that will be emitted in Jami according to the chosen parameters. We also have an overview of the visual quality.

* * *

1080p / 300 kbit/s / CRF28
![image](encoder01.png)
1080p / 300 kbit/s / CRF38
![image](encoder02.png)

Visual comparison (CRF28 a gauche / CRF38 a droite)
https://nextcloud.savoirfairelinux.com/index.php/s/twzPmT3MRPjQ3b7

1080p / 1.5 Mbps / CRF22
![image](encoder03.png)
1080p / 1.5 Mbit/s / CRF30
![image](encoder04.png)

Visual comparison (CRF22 left / CRF30 right)
https://nextcloud.savoirfairelinux.com/index.php/s/XK6Ef3rwBFriRXT

 1080p / 6 Mbps / CRF17
![image](encoder05.png)
 1080p / 6 Mbit/s / CRF23
![image](encoder06.png)

Visual comparison (CRF17 on the left / CRF23 on the right)
https://nextcloud.savoirfairelinux.com/index.php/s/RG3TksjQKtMorW5

* * *

720p / 300 kbps / CRF28
![image](encoder07.png)
720p / 300 kbit/s / CRF38
![image](encoder08.png)
Visual comparison (CRF28 left / CRF38 right)
https://nextcloud.savoirfairelinux.com/index.php/s/Z4T6EzGqyyWTJ9f

720p / 1.5 Mbps / CRF22
![image](encoder09.png)
720p / 1.5 Mbit/s / CRF30 (Test with reduced CRF)
![image](encoder10.png)

Visual comparison (CRF22 left / CRF30 right)
https://nextcloud.savoirfairelinux.com/index.php/s/H2Z5NQT3zJ9wzod

720p / 6 Mbps / CRF17
![image](encoder11.png)
720p / 6 Mbit/s / CRF23
![image](encoder12.png)

Visual comparison (CRF17 left / CRF23 right)
https://nextcloud.savoirfairelinux.com/index.php/s/gACSARfHjAKqgRR

* * *

436p / 300 kbps / CRF28
![image](encoder13.png)
436p / 300 kbit/s / CRF38
![image](encoder14.png)

Visual comparison (CRF28 left / CRF38 right)
https://nextcloud.savoirfairelinux.com/index.php/s/gj9CpQox5fiaWMT

436p / 1.5 Mbps / CRF22
![image](encoder15.png)
436p / 1.5 Mbit/s / CRF30
![image](encoder16.png)

Visual comparison (CRF22 left / CRF30 right)
https://nextcloud.savoirfairelinux.com/index.php/s/BeCaqs2rMRpNQwS

436p / 6 Mbps / CRF17
![image](encoder17.png)
436p / 6 Mbit/s / CRF23
![image](encoder18.png)

Visual comparison (CRF17 left / CRF23 right)
https://nextcloud.savoirfairelinux.com/index.php/s/KLeXq9NeLKTfyqs
