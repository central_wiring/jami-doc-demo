Certificates
=====================









https://www.linuxjournal.com/content/understanding-public-key-infrastructure-and-x509-certificates







encoding is important: if there were multiple ways to encode the data
in the certificate, as there might be using BER, the hash might assume
several different values. By using DER, you guarantee that the values
are encoded and decoded consistently into the same series of bytes. If
a single byte changes, a different hash would be created and the
verification








signature from a
certificate authority, which is most often a self-signature.



account:


-   Signed by a CA (from an organization or self-signed).
-   The subject UID field must be the hexadecimal form of the JamiId.
-   The issuer UID field must be the hexadecimal form of the issuer
	public key fingerprint (CA).
-   Random RSA key-pair of at least 4096-bits long.


The subject UID field of the account certificate must be the hexadecimal
form of the public key fingerprint. The issuer UID field must be the
hexadecimal form of the issuer public key fingerprint.

device:


-   This is the identity of one specific device used to run Jami.
-   One per device.
-   Random and 4096-bits long.
-   The SHA-1 fingerprint of the public key becomes the **DeviceId**.
-   Must be signed by the private key that created the Jami certificate.
-   The subject UID field must be the hexadecimal form of the DeviceId.
-   The issuer UID field must be the hexadecimal form of the issuer
	public key fingerprint (JamiId).
