Jami identifiers
----------------

There are many identifiers in Jami. We need to unify the naming of these
identifiers between all implementations. This page reference various kind of
identifiers used in Jami with relevant examples.

-   **Jami Infohash** or **Jami public key fingerprint** : a public key
	fingerprint such as `3d1112ab2bb089370c0744a44bbbb0786418d40b`
-   **Registered name** : a username associated to a Jami Infohash on
	the blockchain such as `jeandupont`
-   **URI** : a Jami or SIP URI such as
	`ring:3d1112ab2bb089370c0744a44bbbb0786418d40b` or `ring:jeandupont`
	or `<sip:nnnnn@host:5060>`. Must be compliant with [rfc3986](https://tools.ietf.org/html/rfc3986). If it's a SIP URI, it must be compliant with [rfc3261#19.1](https://tools.ietf.org/html/rfc3261#section-19.1).
-   **Canonical URI** : `ring:3d1112ab2bb089370c0744a44bbbb0786418d40b` or `sip:nnnnn@host:5060`. The most simplified form of the URI. Registered name must be resolved, doesn't include <> brackets or display name. Prefixed with the scheme (`ring:` or `sip:` or `sips:`).
-   **User ID**: registered name (preferred) or public key fingerprint. User-facing identifier for an account public key.
-   **Display name** or **Profile name** : an editable user-defined
	profile name such as `Jean Dupont`.

When displaying a contact:

```
 _____
|photo|   Display name or User ID
|_____|   User ID
```

-   If Display name is empty, User ID is shown instead
-   If both lines have the same content, only the first line is
	displayed
-   If no photo is available and a registered name (ring) or display name (sip) is available, the first letter of this name can be used to generate a placeholder. Otherwise a generic placeholder is used.
-   If no photo is available, a placeholder with an Canonical URI-specific background color can be used:

```java
final int[] contactColors = {
	color.red_500, color.pink_500,
	color.purple_500, color.deep_purple_500,
	color.indigo_500, color.blue_500,
	color.cyan_500, color.teal_500,
	color.green_500, color.light_green_500,
	color.grey_500, color.lime_500,
	color.amber_500, color.deep_orange_500,
	color.brown_500, color.blue_grey_500
};

int generateAvatarColor(String canonicalUri) {
	if (isEmpty(canonicalUri))
		return R.color.grey_500;
	String h = md5(canonicalUri);
	if (h == null)
		return R.color.grey_500;
	int colorIndex = Integer.parseInt(h.charAt(0) + "", 16);
	return contactColors[colorIndex % contactColors.length];
}
```

Color values are from the material palette: https://material.io/tools/color


![références_couleurs_jami](color_reference.png)
