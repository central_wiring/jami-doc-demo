.. Jami documentation master file, created by
   sphinx-quickstart on Sat Feb 13 12:46:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Home
==============================================

.. image:: https://jami.net/assets/images/logo-jami.svg
  :alt: Jami Logo

This is the documentation for Jami, a free and private communication
platform.

This documentation is community-driven and :doc:`anyone can
contribute<guides/how-to-contribute-to-this-documentation>`!

.. toctree::
   :maxdepth: 2

   general/index
   guides/index
   build-instructions/index
   technical/index
   extra/index
   changelog
