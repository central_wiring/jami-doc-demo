# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Jami'
copyright = '2021, SFL Employees and Contributors'
author = 'SFL Employees and Contributors'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['recommonmark',
              # Convert SVG images when doing a latex build
              'sphinx.ext.imgconverter',
              # Auto-generate section labels (for linking between documents in a portable way)
              'sphinx.ext.autosectionlabel',
              ]

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}

# ignore warnings about duplicate links; often we want to link to something multiple times in a doc (e.g. the FAQ)
suppress_warnings = ['autosectionlabel.*']

# link to the "Swarms" heading in the Technical Overview document like this: technical-overview#swarms (or markdown: technical-overview:swarms)
autosectionlabel_prefix_document = True

#
autosectionlabel_maxdepth = 4


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    'navigation_depth': 3,
    'sticky_navigation': False,
    'collapse_navigation': False,
}

html_sidebars = { '**': ['globaltoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html'] }

# html_sidebars = { '**': ['globaltoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html'] }

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for LaTeX pdf output ---------------------------------------------

latex_engine = 'xelatex'
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    'papersize': 'a4paper',
    # The font size ('10pt', '11pt' or '12pt').
    'pointsize': '10pt',

    'fontpkg': r'''
\setmainfont{Linux Libertine}
''',
    'preamble': r'''
\setcounter{tocdepth}{2}

''',
    'fncychap': r'\usepackage[Rejne]{fncychap}',
  'sphinxsetup': \
        'hmargin={0.7in,0.7in}, vmargin={1in,1in}, \
        verbatimwithframe=false,'
}
latex_show_urls = 'footnote'
