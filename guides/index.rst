##########
Guides:
##########

These are how-to guides that should `follow this format
<https://documentation.divio.com/how-to-guides/>`_.

.. toctree::
   :maxdepth: 1

   how-to-make-a-bug-report
   how-to-contribute-to-this-documentation
   how-to-submit-a-patch
   how-to-set-up-a-turn-server
   how-to-set-up-a-dhtproxy-or-bootstrap-server
