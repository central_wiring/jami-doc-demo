# How to Submit a Patch

These are the steps

## Setting up git and ssh

(**to work smoothly with Gerrit.**)


first see:

* [Creating a gerrit review](https://git.jami.net/savoirfairelinux/ring-project/wikis/tutorials/Working-with-gerrit#to-create-the-review)

## SSH setup

https://review.jami.net/Documentation/user-upload.html#ssh

1. Generate a personal dedicated public / private key set.
```bash
ssh-keygen -t rsa -f ~/.ssh/jami_gerrit_review_rsa`
```

Your identification has been saved in `jami_gerrit_review_rsa`. Your public key has been saved in `jami_gerrit_review_rsa.pub.`


2. Tell gerrit your public key
   1. Login to [gerrit](https://review.jami.net) via your Gitlab account (Sign-in=>OAuth Gitlab)
   2. Follow the ssh key instructions found from (your) user options [settings](https://review.jami.net/settings/)

3. Set up your local ssh (via `ssh-add` or in `~/.ssh/config`)
4. Test all of the above (ssh on the service or just try to clone a repo on gerrit via ssh)


## Your Repository

This *assumes* you have moved any existing project clone out of the way.

1. Clone a (fresh) copy of the project. `git clone ssh://USERNAME@review.jami.net:29420/PROJECT_NAME_GOES_HERE.git` eg `git clone ssh://foo@review.jami.net:29420/ring_project.git`.
2. Configure this clones local .git/config (optional)
3. Generate commit Change-Ids
aka: commit-msg hook script

A shell script, to be installed to .git/hooks/commit-msg.


The script creates a unique Change_Id:hash for each commit made from your repository.


- The first time you attempt a _push_ to review `git push origin HEAD:refs/for/master`,
  gerrit will notice a lack of Change-Id.

- Gerrit will also present a secure copy (scp) string.
  Use *that* to download a copy of .git/hooks/commit-msg.


References
----------

+ [Some Gritty Details](https://review.jami.net/Documentation/user-upload.html#_gritty_details)



Original author: *Peter Gossner*
