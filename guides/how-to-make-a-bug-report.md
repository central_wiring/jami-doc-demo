How to Make a Bug Report
----------------

**Note: We are currently a few devs active on the project. We can’t
answer and tags each issues opened, but we read it. A good issue provide
an important feedback and thank you for that, any feedback is
appreciated.**

Setup your environment
~~~~~~~~~~~~~~~~~~~~~~

-  Be ready for data loss. Backup your account and link your account to
   as many devices as possible.
-  Install the latest version (or even a beta version) of Jami. It is
   useless to report bugs on older versions.

How to report a bug
~~~~~~~~~~~~~~~~~~~

0. Create an account on `our git
repository <https://git.jami.net/users/sign_in>`__ (if it is not already
done)

1. Choose the right project to post your issue in: \* `The Android
client <https://git.jami.net/savoirfairelinux/ring-client-android>`__ \*
`The Windows
client <https://git.jami.net/savoirfairelinux/ring-client-windows>`__ \*
`The Linux
client <https://git.jami.net/savoirfairelinux/ring-client-gnome>`__ \*
`The iOS
client <https://git.jami.net/savoirfairelinux/ring-client-ios>`__ \*
`The macOS
client <https://git.jami.net/savoirfairelinux/ring-client-macosx>`__ \*
`The Jami project in general (or if you are not
sure) <https://git.jami.net/savoirfairelinux/ring-project>`__ \* `If you
know what you are doing you may choose one of the other
projects <https://git.jami.net/>`__

2. If you have multiple issues, please file separate bug reports. It
will be much easier to track bugs that way.

3. Title is an explicit summary of the bug (e.g.: header bar is too big
due to icon size)

4. Figure out the steps to reproduce the bug:

-  If you have precise steps to reproduce it — great! — you’re on your
   way to creating a useful bug report.
-  If you can reproduce occasionally, but not after following specific
   steps, you must provide additional information for the bug report to
   be useful.
-  If you can’t reproduce the problem, there’s probably no use in
   reporting it, unless you provide unique information about its
   occurrence.
-  If the bug leads to other bugs afterward that you can’t reproduce
   some other way, there’s probably no use in reporting it.

5. Make sure your software is up to date. Ideally, test an
in-development version to see whether your bug has already been fixed

6. Try to isolate from environment and reproduce (ie: test on multiple
devices)

7. Describe your environment(s) by specifying the following:

-  OS version
-  precise device model (important for mobile devices)
-  if you are using a beta version
-  what build you are using (from ring.cx, F-droid, Play Store, App
   store, you own…). If you have built your own version of Ring, please
   specify the exact dring version (LibRing or Daemon) and client
   version (You can obtained it with ``dring -v`` and ``jami-gnome -v``.
   But note that our packages are updated quite often) and the Git
   commit.
-  network conditions: Are both devices on the same local network?
   Different networks? Is one or both behind NAT? Are you using LTE?
   Wifi?
-  other elements if needed: SIP provider, hardware…

Writing a clear summary
~~~~~~~~~~~~~~~~~~~~~~~

How would you describe the bug using approximately 10 words? This is the
first part of your bug report a developer will see.

A good summary should quickly and uniquely identify a bug report. It
should explain the problem, not your suggested solution.

::

   Good: "Cancelling a file transfer crashes Ring"
   Bad: "Software crashes"

::

   Good: "All calls hang up after 32 seconds"
   Bad: "Not able to call my friends"

Writing precise steps to reproduce
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  How can a developer reproduce the bug on his or her own device?

Steps to reproduce are the most important part of any bug report. If a
developer is able to reproduce the bug, the bug is very likely to be
fixed. If the steps are unclear, it might not even be possible to know
whether the bug has been fixed. We are totally aware that some bugs may
look obvious to you, but they are probably related to your environment.
The more precise you are, the quicker the bug is fixed.

-  What should you include in a bug report?

Indicate whether you can reproduce the bug at will, occasionally, or not
at all. Describe your method of interacting with Ring in addition to the
intent of each step. After your steps, precisely describe the observed
(actual) result and the expected result. Clearly separate facts
(observations) from speculations.

Good :
^^^^^^

I can always reproduce by following these steps:

::

   1. Start Ring by clicking on the desktop icon
   2. Start a new conversation with anyone
   3. Click file transfer icon

   Expected results: A window opens and ask me to choose a file to send.
   Actual results: When I click the file transfer icon, nothing happens.

Bad :
^^^^^

::

   Try to transfer a file
   It doesn't work.

Obtained Result
~~~~~~~~~~~~~~~

Please include:

-  The daemon (or LibRing) and client debug logs.
-  The core dump if one was produced.

Expected Result
~~~~~~~~~~~~~~~

It’s a description of expected or desired behaviour.

Providing additional information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following information is requested for most bug reports. You can
save time by providing this information below the Expected results.

Logs
^^^^

On GNU/Linux
''''''''''''

Classic logs (by default logs only >= warning are loggued):

::

   journalctl --since "24h ago" | grep dring

Full log: Since the Ring GUI and daemon are separated processes, the
easiest way to get logs from both is to start them one at a time,
manually.

1. Ensure that no ring client or daemon instances are running with
   ``ps aux | grep ring``

   -  Ring may still be running even if no windows are open depending on
	  your preferences.
   -  If either client or daemon are running, kill them with
	  ``kill PID``

2. On one terminal, start the daemon with ``dring -d -c``

   -  This executable is normally not in the PATH, and on the Ubuntu
	  packages, it is located at
	  ``/usr/lib/x86_64-linux-gnu/dring -d -c`` or
	  ``/usr/lib/ring/dring -d -c``

3. In another terminal, start the client with (here is a Gnome example)
   ``jami-gnome -d``

For getting a backtrace, you can run the program inside gdb:

`gdb -ex run jami-gnome\ ``or``\ gdb -ex run –args /usr/lib/ring/dring
-cd\`

When it does crash, you can type ``bt`` then press **Enter**. And copy
the backtrace to the issue. (or ``thread apply all bt`` is event better)

On Mac OS
'''''''''

Open the Terminal app and launch Ring with
``/Applications/Ring.app/Contents/MacOS/Ring``

-  Open Console
-  Click on “All messages”
-  Search field: “Jami”

On Android (debug builds only)
''''''''''''''''''''''''''''''

-  You need to have adb setup on your computer.
-  Launch Ring on your smartphone and then execute
-  ``adb logcat *:D | grep \``\ adb shell ps \| egrep ‘cx.ring’ \| cut
   -c10-15\` > logring.txt\`
-  You now have a file containing the log of the client

For Windows
'''''''''''

Open a terminal(cmd.exe) and launch Jami.exe with the following options:

-  ``-d`` to open a separate console window to receive logs
-  ``-f`` to write logs to ``%localappdata%\jami\jami.log``
-  ``-c`` to print logs directly to the Visual Studio debug output
   window
