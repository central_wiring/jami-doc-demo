# Building Jami for iOS

After installing [Brew](https://brew.sh) and Python3 (brew install
python3):

```bash
git clone https://review.jami.net/ring-project
cd ring-project
./make-ring.py --init
./make-ring.py --dependencies --distribution iOS
./make-ring.py --install --distribution iOS
cd client-ios/Ring && ./fetch-dependencies.sh && cd ..
xcodebuild build -project Ring/Ring.xcodeproj/ -configuration "Release" -arch "x86_64" -destination "platform=iOS Simulator,name=iPhone $DATE,OS=11" -sdk iphonesimulator11.0 VALID_ARCHS="x86_64"


```
it could be useful to do this if an error occurs:
```bash
ln -s /usr/local/opt/gettext/bin/autopoint /usr/local/bin
```
if you get build errors linked to swift, you should upgrade swiftgen:
```bash
brew upgrade swiftgen
```
