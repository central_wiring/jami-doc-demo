# Building Jami on macOS

Simply follow the instructions in the README of the [master
repository](https://git.jami.net/savoirfairelinux/ring-project#on-osx).
