# Building Jami on Android

+ Please make sure Android SDK and NDK are installed, and their paths
are properly set. If you need more information, please visit
<https://github.com/savoirfairelinux/ring-client-android>
+ These instructions are for Ubuntu, check equivalent if you use a different distribution

## Prepare environment

-   Install Java JDK 7 or 8 (Oracle or OpenJDK)

<http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>

```bash
sudo apt install openjdk
```

<!-- -->

-   Install Android Studio:
	<https://developer.android.com/studio/index.html>

	You can install all dependecies like ndk (side by side), lldb with android-studio in settings > appearance & behavior > system settings > android sdk > sdk tools

<!-- -->

-   Install the Android NDK:
	<https://developer.android.com/ndk/downloads/index.html>

<!-- -->

-   Install required tools

<!-- -->

```bash
sudo apt install autoconf automake libtool autopoint swig python
```

Add these variables to your bash profile:

```bash
export JAVA_HOME=<path_to_java_JDK>
export ANDROID_HOME=<path_to_root_folder_sdk>
export ANDROID_SDK=$ANDROID_HOME
export ANDROID_NDK=<path_to_root_folder_ndk>
export ANDROID_NDK_ROOT=$ANDROID_NDK
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_NDK:$JAVA_HOME/bin
```

ie for Ubuntu :

```bash
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/
export ANDROID_HOME=/home/{username}/Android/Sdk
export ANDROID_SDK=/home/{username}/Android/Sdk
export ANDROID_NDK_ROOT=/home/{username}/Android/Sdk/ndk-bundle/
export ANDROID_NDK=/home/{username}/Android/Sdk/ndk-bundle/
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_NDK:$JAVA_HOME/bin
```

## Build and install locally

-   Clone whole project repository

<!-- -->

```bash
git clone --recurse-submodules https://review.jami.net/ring-project
```

-   Initialize project

<!-- -->
```bash
cd ring-project
./make-ring.py --init --distribution=Android
```


-   Compile

<!-- -->

```bash
ANDROID_ABI="armeabi-v7a arm64-v8a" ./make-ring.py --install --distribution=Android
```

**Output**: You can find the *.apk* file in the
*./client-android/ring-android/app/build/outputs* folder.

**Errors**
- ```configure: error: source directory already configured; run "make distclean" there first```

	It means you build for another distro like x86_64-pc-linux-gnu and builds are conflicting. The simpliest solution is to make another ring-project only for Android to avoid this conflict.

## Troubleshooting

-   Check case in your sdk path. Since api 26,
	/home/user/Android/**s**dk have become /home/user/Android/**S**dk

<!-- -->

-   You can check each path with *echo*. e.g : echo $JAVA\_HOME

<!-- -->

-   `sudo` will use root's bash profile. And you don't need it.

<!-- -->

-   If build fails, you can try to clean contribs with:

```bash
cd ring-project/ && git clean -xdf
cd ../daemon && git clean -xdf
```

- Makeinfo issue
	makeinfo: command not found
	WARNING: 'makeinfo' is missing on your system.
	**Solution**:   Install texinfo package containing makeinfo dep.

<!-- -->

- Unable to locate tools.jar
	**Solution**:   Your java installation is not pointing to a JDK.
					Install one, or make JAVA_HOME point to it.

<!-- -->

- When building the apk error in build-tools
	error while loading shared libraries: libstdc++.so.6
	**Solution**:   Install lib32stdc++6 lib32z1-dev

<!-- -->

- When compiling on Fedora
	error while loading shared libraries: libtinfo.so.5: cannot open shared object file: No such file or directory
	**Solution***: sudo dnf install ncurses-compat-libs

<!-- -->

- When building, you may get a Gradle error. You should install Gradle 5.4.1 by running apt-get install or installing directly on android-studio.
