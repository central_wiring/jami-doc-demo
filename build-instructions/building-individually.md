# Building Components Individually

If you are building each component individually, simply ``git clone``
each repository and follow the instructions in its README. Do it in
the following order:

  * [jami-daemon](https://git.jami.net/savoirfairelinux/ring-daemon)
  * [libjamiclient](https://git.jami.net/savoirfairelinux/ring-lrc) (not used on Android)
  * A client from <https://git.jami.net/savoirfairelinux>, such as the
	[QT](https://git.jami.net/savoirfairelinux/jami-client-qt),
	[GNOME](https://git.jami.net/savoirfairelinux/ring-client-gnome),
	or
	[macOS](https://git.jami.net/savoirfairelinux/ring-client-macosx)
	clients.
