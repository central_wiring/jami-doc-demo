# Building Jami on Windows

Follow the instructions in the QT client's
[repository](https://git.jami.net/savoirfairelinux/jami-client-qt#building-on-native-windows).
