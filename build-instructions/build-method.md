# Which Build Method to Use

The following flow chart should help you decide whether to build from
the [master repository](https://git.jami.net/savoirfairelinux/ring-project) or not:

![flowchart](build_method_flowchart.png)
