###################
Build Instructions
###################

A working Jami setup consists of three ingredients:

  * `jami-daemon <https://git.jami.net/savoirfairelinux/ring-daemon>`_, the core program,
  * optionally, `libjamiclient
    <https://git.jami.net/savoirfairelinux/ring-lrc>`_, a library used
    by some of the clients
  * A client, such as `the QT client
    <https://git.jami.net/savoirfairelinux/jami-client-qt>`_ which
    provides the user interface


**There are two ways to build Jami**:

   1. Build each part individually. They must be built in the order given above.
   2. Build Jami using the :doc:`master repository
      <master-repository>`, which provides a script to build all of
      the components at once. This is easier for most people.

Whichever method you choose, you should follow the build instructions
in the README or INSTALL file of your chosen repository.

.. toctree::
   :maxdepth: 1

   build-method
   master-repository
   building-individually
   windows
   linux
   macos
   android
   ios
