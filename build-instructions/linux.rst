Building Jami on Linux
======================

Simply follow the instructions in the README of the `master
repository <https://git.jami.net/savoirfairelinux/ring-project>`__.

Alternatively, follow the build instructions for :doc:`each individual
component <building-individually>` if you choose to build them
separately.

Dependencies
------------

Jami-daemon provides some dependencies in its ``contrib`` directory,
along with instructions for compiling them. However, some dependencies
should be installed with your package manager.

Fedora
~~~~~~

.. code:: bash

   sudo yum groupinstall group "Development Tools" "Development Libraries"
   sudo yum install gcc-c++ yasm intltool libyaml-devel alsa-lib-devel pulseaudio-libs-devel libsamplerate-devel dbus-c++-devel pcre-devel gsm-devel speex-devel speexdsp-devel expat-devel qttools5-dev libsndfile-devel gnutls-devel gettext-devel cmake libtool systemd-devel uuid-devel libXfixes-devel jsoncpp-devel autoconf-archive qt5-qtbase-devel qt5-qttools-devel

For video support, you’ll also need ffmpeg, which is only available in
the RPMfusion repository as described here
http://rpmfusion.org/Configuration

Then install ffmpeg:

.. code:: bash

   sudo yum install ffmpeg-devel

To build and run the tests with make check, you’ll also need

.. code:: bash

   sudo yum install cppunit-devel cppcheck sipp dbus

Debian-based
~~~~~~~~~~~~

Building dependencies/instructions for Debian, Ubuntu, Crunchbang Linux,
etc.

.. code:: bash

   sudo apt-get install autoconf autoconf-archive automake autopoint cmake libpulse-dev libsamplerate0-dev libgsm1-dev libspeex-dev libtool libdbus-1-dev libasound2-dev libopus-dev libspeexdsp-dev libexpat1-dev libpcre3-dev libyaml-cpp-dev libboost-dev libdbus-c++-dev qttools5-dev libsndfile1-dev libsrtp-dev libjack-dev libupnp-dev libavcodec-dev libavformat-dev libswscale-dev libavdevice-dev libudev-dev yasm uuid-dev libgnutls28-dev libjsoncpp-dev libvdpau-dev libva-dev qtbase5-dev qttools5-dev and qttools5-dev-tools

If you want to run tests (e.g. when setting up a new VM on Jenkins), you
must install cppunit and sipp

.. code:: bash

   sudo apt-get install libcppunit-dev sip-tester dbus

For H.264 support, you’ll need

.. code:: bash

   sudo apt-get install libavcodec-extra-*
