# Using the Master Repository

You must first install [Git](https://git-scm.com/).

Run the following command to download the project:

```bash
$ git clone https://review.jami.net/ring-project
```

Then follow the instructions in the [master repository
README](https://git.jami.net/savoirfairelinux/ring-project).

## Updating your copy


Due to high activity on the other projects, the Git submodules may need to
be manually updated next time you rebuild everything:

```
$ cd ring-project
$ git submodule update --init daemon lrc client-gnome
```
