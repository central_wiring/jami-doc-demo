# Feature Requests

This page exists to classify features request coming from users
feedback (to avoid to let the ticket open for years). (under
construction, will sort the tickets)

## Planned/In progress

+ Group chat. Yeah we know it's missing but it's currently in progress (https://git.jami.net/groups/savoirfairelinux/-/epics/2)
+ Syncing history (linked to group chat support)
+ Read notifications (already available on some platforms)

## Wanted but not planned

+ A web interface. Because installing an app can be boring. Joining a conference via a link can be cool. For now, nobody is working on it.
+ Push to talk, voice detection
+ Ability to start a call without the camera (https://git.jami.net/savoirfairelinux/ring-project/issues/428)

## Can be implemented, contributions welcome (or will take months/years to come)

+ Support for Panic buttons (https://git.jami.net/savoirfairelinux/ring-project/issues/623)
+ Matrix bridge
+ Full tor support or other alternatives such as lokinet (https://git.jami.net/savoirfairelinux/ring-project/issues/922, https://git.jami.net/savoirfairelinux/ring-project/issues/622, https://git.jami.net/savoirfairelinux/ring-project/issues/495), i2p (https://git.jami.net/savoirfairelinux/ring-project/issues/630)
+ Bluetooth support (https://git.jami.net/savoirfairelinux/ring-project/issues/774)
+ Secret based turn server (https://git.jami.net/savoirfairelinux/ring-project/issues/886)
+ Option to compress files before sending it (https://git.jami.net/savoirfairelinux/ring-client-android/issues/720)
+ Trim recorded clips before sending
+ Spell checking support https://git.jami.net/savoirfairelinux/ring-client-gnome/issues/1169
+ Echo bot to test audio https://git.jami.net/savoirfairelinux/ring-project/issues/392
+ Search into system contacts (https://git.jami.net/savoirfairelinux/ring-client-gnome/issues/1191, https://git.jami.net/savoirfairelinux/ring-client-gnome/issues/829, etc)
+ Support for markdown https://git.jami.net/savoirfairelinux/ring-lrc/issues/416
+ Handle click on jami:uri system wide https://git.jami.net/savoirfairelinux/ring-project/issues/653

## Depends on mass changes

+ Emoticon Message Reactions (https://git.jami.net/savoirfairelinux/ring-project/issues/1034) (need to wait for group chat)

## Others

+ A thunderbird plugin (https://git.jami.net/savoirfairelinux/ring-project/issues/516)
+ OpenAlias (https://git.jami.net/savoirfairelinux/ring-project/issues/928)
+ CMIS integration (https://git.jami.net/savoirfairelinux/ring-project/issues/455)
+ Sound safety (https://git.jami.net/savoirfairelinux/ring-project/issues/441)
+ Ability to see multiple chats at the same time (https://git.jami.net/savoirfairelinux/ring-client-gnome/issues/909)
+ Vocoder option (https://git.jami.net/savoirfairelinux/ring-client-gnome/issues/957)
+ Socks5 support https://git.jami.net/savoirfairelinux/ring-project/issues/430
+ Cardbook integration https://git.jami.net/savoirfairelinux/ring-project/issues/383
+ Multiple instances running: https://git.jami.net/savoirfairelinux/ring-project/issues/629
+ Whiteboard https://git.jami.net/savoirfairelinux/ring-daemon/issues/181
