.. image:: https://jami.net/assets/images/logo-jami.svg

Introduction
============

Jami is a free and private communication platform.

It’s Free/Libre, end-to-end encrypted, and requires no central
authority.

Features include text chats, voice and video calls, screen sharing, file
sharing, conference calls, and group chats (:ref:`coming
soon <general/technical-overview:swarms>`). Jami can also function as a
regular SIP client.

Jami works on Windows, macOS, Linux, iOS, and Android. Multiple devices
can be linked to one account. No personal information is required to
create an account.

How does it work?
-----------------

Jami uses a `distributed hash
table <https://en.wikipedia.org/wiki/Distributed_hash_table>`__ to
connect peers. Jami accounts are asymmetric x.509 certificates generated
by the GnuTLS library. Calls are made over the SIP protocol after
negotiating a secure connection using TLS.

For more in-depth information, see :doc:`the Technical
Overview <technical-overview>`.

Who makes Jami?
---------------

This project is led by `Savoir-faire
Linux <https://www.savoirfairelinux.com/en/>`__ – a Canadian/Quebecois
GNU/Linux consulting company – and is supported by a global community.

Jami is Free software: its sources are licensed under the
`GPLv3+ <https://www.gnu.org/licenses/gpl-3.0.html>`__.
