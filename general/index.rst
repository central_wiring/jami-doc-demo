##########
General
##########

This chapter introduces Jami and tries to answer the most common
questions.

.. toctree::
   :maxdepth: 1

   introduction
   faq
   technical-overview
   all-features-by-client
   feature-requests
   troubleshooting
